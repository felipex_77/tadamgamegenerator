# README #

The aim of the project is to create customable rpg 2D game according to client specifications.

### Technologies ###
* Java 11
* JavaFX
* Gradle

### How do I get set up? (Intellij IDEA) ###
* Clone project :  
  `git clone https://Felipex_77@bitbucket.org/felipex_77/tadamgamegenerator.git`
* Open project in Intellij IDEA
* Make sure you are using java 11 as your SDK in '_**File -> Project Structure**_'
* Build gradle dependencies :  
  `gradlew build`  
  or just click '_**Tasks -> build -> build**_' at gradle right-side bar (Intellij)

### How do I run the app? ###
* Use command  
`gradlew run`  
* or click  
'_**Tasks -> application -> run**_' at gradle right-side bar (Intellij)

### Notes ###
* Above configuration make look different depending on what IDE you are using.

### Useful links/resources ###

* [JavaFX Scene builder](https://gluonhq.com/products/scene-builder/)
* [CSS Styling](https://openjfx.io/javadoc/18/javafx.graphics/javafx/scene/doc-files/cssref.html)
