package com;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.EntityFactory;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.Spawns;
import com.almasb.fxgl.entity.components.BoundingBoxComponent;
import com.almasb.fxgl.entity.components.CollidableComponent;
import com.almasb.fxgl.entity.components.IrremovableComponent;
import com.almasb.fxgl.physics.BoundingShape;
import com.almasb.fxgl.physics.HitBox;
import com.components.*;
import com.components.credits.Credit;
import com.components.credits.ExpCondition;
import com.components.credits.ItemCondition;
import com.components.items.CupItem;
import com.components.items.ExpItem;
import com.components.items.PickOnlyItem;
import com.components.items.HealthPotionItem;
import com.components.items.Item;
import com.configuration.DoorsPool;
import com.configuration.ItemsPool;
import com.configuration.LevelsManager;
import com.configuration.NpcsPool;
import com.interactive.Interactive;
import com.model.DoorInfo;
import com.model.ItemInfo;
import com.model.npc.NpcInfo;
import com.model.npc.NpcInteractiveBuilder;
import com.model.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.Arrays;

import static com.almasb.fxgl.dsl.FXGL.*;
import static com.data.EntityName.*;
import static com.data.EntityType.*;

/***
 * class responsible for entities creation.
 */
public class GameElementsFactory implements EntityFactory {
    PlayerInfo playerInfo;

    public GameElementsFactory(PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
    }

    @Spawns(ENTITY_WALL)
    public Entity wall(SpawnData data) {
        return entityBuilder(data)
                .bbox(new HitBox(BoundingShape.box((int) data.get("width"), (int) data.get("height")))) // casting is needed
                .type(WALL)
                .with(new CollidableComponent(true))
                .build();
    }

    @Spawns(ENTITY_NPC)
    public Entity npc(SpawnData data) {
        // tags, reward, questionPool
        NpcInfo npcInfo = getNpcInfo(data.get("name"));
        Interactive npcInteractive = NpcInteractiveBuilder.buildNpcInteractive(npcInfo);
        Credit credit = buildCredit(npcInfo.requiredItems, npcInfo.requiredExp);
        Entity result = entityBuilder(data)
                .type(NPC)
                .viewWithBBox(npcInfo.imageAsset)
                .with(new CollidableComponent(true))
                .with(new InteractionComponent(npcInteractive, credit))
                .build();
        BoundingBoxComponent bBox = result.getBoundingBoxComponent();
        HitBox newHitBox = new HitBox(BoundingShape.circle(0.45 * bBox.getWidth()));
        bBox.clearHitBoxes();
        bBox.addHitBox(newHitBox);
        result.setReusable(true);
        return result;
    }

    private Credit buildCredit(String[] requiredItems, short requiredExp) {
        Credit result = new Credit(new ExpCondition(requiredExp));
        Arrays.stream(requiredItems)
                .map(ItemCondition::new)
                .map(Credit::new)
                .forEach(result::addPrerequisite);
        return result;
    }

    private NpcInfo getNpcInfo(String npcName) {
        return ((NpcsPool) getWorldProperties().getValue("npcs")).getNpc(npcName).get();
    }


    @Spawns(ENTITY_PLAYER)
    public Entity newPlayer(SpawnData data) {
        if (GameRunner.getPlayer() != null) {
            return entityBuilder(data)
                    .type(PLAYER)
                    .build();
        }

        int maxExpPoints = ((LevelsManager) getWorldProperties().getValue("levels")).getMaxExpForLevel();
        EqComponent eqComponent = new EqComponent(16);

        Entity player =  entityBuilder(data)
                .type(PLAYER)
                .view(playerInfo.getTextureImage())
                .bbox(BoundingShape.circle(playerInfo.getCollisionRadius()))
                .with(new CollidableComponent(true))
                .with(new PlayerComponent())
                .with(new HealthComponent(playerInfo.getMaxHealth()))
                .with(new ExpComponent(maxExpPoints))
                .with(eqComponent)
                .with(new IrremovableComponent())
                .with(new StatisticsComponent())
                .zIndex(1)
                .build();

        GameRunner.setPlayer(player);
        return player;
    }

    @Spawns(ENTITY_DOOR)
    public Entity newDoor(SpawnData data) {
        DoorInfo doorInfo = getDoorInfo(data.get("name"));
        String assetName = doorInfo.getAssetName();
        Credit doorCondition = buildCredit(doorInfo.getRequiredItems(), doorInfo.getRequiredExp());
        return entityBuilder(data)
                .type(DOOR)
                .viewWithBBox(assetName == null ? new Rectangle((int) data.get("width"), (int) data.get("height"), Color.BLACK) : texture(assetName)) // casting is needed
                .with(new DoorComponent(doorInfo.getEnteredRoom(), doorInfo.getDirection(), doorCondition))
                .with(new CollidableComponent(true))
                .build();
    }

    private DoorInfo getDoorInfo(String doorName) {
        return ((DoorsPool) getWorldProperties().getValue("doors")).getDoor(doorName).get();
    }

    @Spawns(ENTITY_EXP)
    public Entity newExpItem(SpawnData data) {
        ItemInfo info = getItemInfo(data.get("name"));
        return buildItemFromClass(data, new ExpItem(info));
    }

    @Spawns(ENTITY_HEALTH)
    public Entity newHealthItem(SpawnData data) {
        ItemInfo info = getItemInfo(data.get("name"));
        return buildItemFromClass(data, new HealthPotionItem(info));
    }

    @Spawns(ENTITY_PICK_ITEM)
    public Entity newGoldItem(SpawnData data) {
        ItemInfo info = getItemInfo(data.get("name"));
        return buildItemFromClass(data, new PickOnlyItem(info));
    }

    @Spawns(ENTITY_CUP)
    public Entity newCupItem(SpawnData data) {
        ItemInfo itemInfo = getItemInfo(data.get("name"));
        CupInfo cupInfo = ((LevelsManager)getWorldProperties().getValue("levels"))
                .getCupWithNameOnCurrentLevel(itemInfo.getName());
        String[] emptyArray = {};
        Credit cupCondition = buildCredit(emptyArray, (short) cupInfo.getExpRequired());
        return buildItemFromClass(data, new CupItem(itemInfo, cupInfo.getScoreForLevel()), cupCondition);
    }

    private ItemInfo getItemInfo(String itemName) {
        return ((ItemsPool) getWorldProperties().getValue("items")).getItem(itemName).get();
    }

    private Entity buildItemFromClass(SpawnData data, Item item) {
        return buildItemFromClass(data, item, new Credit());
    }

    private Entity buildItemFromClass(SpawnData data, Item item, Credit credit) {
        Entity result = entityBuilder(data)
                .type(ITEM)
                .at(data.getX(), data.getY())
                .viewWithBBox(item.getAssetName())
                .with(new InteractionComponent(item, credit))
                .build();
        result.setReusable(true);
        return result;
    }
}
