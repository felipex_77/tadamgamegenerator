package com;

import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.level.Level;
import com.almasb.fxgl.entity.level.tiled.TMXLevelLoader;

import java.util.HashMap;
import java.util.Map;

import static com.almasb.fxgl.dsl.FXGL.getAssetLoader;
import static com.data.EntityType.PLAYER;

public class GameMap {
    private final Map<String, Level> roomsMap;
    private final TMXLevelLoader levelLoader;

    public GameMap() {
        this.roomsMap = new HashMap<>();
        this.levelLoader = new TMXLevelLoader();
    }

    public Level loadRoom(String roomName) {
        if (!roomsMap.containsKey(roomName)) {
            Level room = getAssetLoader().loadLevel(roomName, levelLoader);
            room.getEntities().forEach(e -> e.setReusable(true));
            roomsMap.put(roomName, room);
        }
        return roomsMap.get(roomName);
    }

}
