package com;

import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.app.scene.FXGLDefaultMenu;
import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.SceneFactory;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.level.Level;
import com.almasb.fxgl.ui.ProgressBar;
import com.collisionHandlers.BlockCollisionHandler;
import com.collisionHandlers.DoorCollisionHandler;
import com.components.*;
import com.components.items.CupItem;
import com.configuration.*;
import com.eventHandlers.RoomChangedHandler;
import com.events.RoomChangeEvent;
import com.model.PlayerInfo;
import com.model.questions.MultipleChoiceQuestion;
import com.model.questions.SingleChoiceQuestion;
import com.ui.*;
import com.util.JsonLoader;
import javafx.beans.property.IntegerProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static com.almasb.fxgl.dsl.FXGL.*;
import static com.data.EntityType.*;
import static com.events.RoomChangeEvent.CHANGE;

public class GameRunner extends GameApplication {
    private GameMap gameMap;
    private PlayerComponent playerComponent;
    private MainMenu mainMenu;
    private Optional<DoorsPool> doorsPool;
    private QuestionsPool questionsPool;
    private Optional<NpcsPool> npcsPool = Optional.empty();
    private ProgressBar expBar;
    private Instant startTime;
    private static Optional<LevelsManager> levelsManager = Optional.empty();
    private static Optional<ItemsPool> itemsPool = Optional.empty();
    private QuestsView questsView = new QuestsView();
    private static Entity player;


    @Override
    protected void onPreInit() {
        super.onPreInit();
        MusicManager.playMenu();
    }

    @Override
    protected void initSettings(GameSettings settings) {
        settings.setWidth(720);
        settings.setHeight(720);
        settings.setTitle("Tadam Game App");
        settings.setVersion("0.1");
        settings.setMainMenuEnabled(true);
        settings.setSceneFactory(new SceneFactory() {
            @Override
            public FXGLMenu newMainMenu() {
                mainMenu = new MainMenu(System.getProperty("user.dir") + "/build/resources/main/assets/example0", List.of("doors.json", "items.json", "levels.json", "npcs.json", "player.json")); // TODO : configuration has changed, we don't need to look for map.json, rooms.json
                return mainMenu;
            }
        });
    }

    @Override
    protected void initInput() {
        onKey(KeyCode.D, () -> playerComponent.right());
        onKey(KeyCode.A, () -> playerComponent.left());
        onKey(KeyCode.W, () -> playerComponent.up());
        onKey(KeyCode.S, () -> playerComponent.down());

        onKeyDown(KeyCode.N, () -> playerComponent.resetNotifications()); // if someone want clues one more time.

        onKeyDown(KeyCode.SPACE, () -> playerComponent.makeInteraction());

        onKeyDown(KeyCode.E, () -> player.getComponent(EqComponent.class).displayMenu());

        onKeyDown(KeyCode.Q, () -> questsView.switchVisibility(playerComponent.getRequiredQuestItems()));

        onKeyDown(KeyCode.F, () -> levelsManager.get().giveUpLevel());
    }

    /***
     * think of it as a main - configuration and initialization of world is here
     */
    @Override
    protected void initGame() {
        PlayerInfo playerInfo = JsonLoader.loadJSON(mainMenu.getPathToConfigurationFolder() + "player.json", PlayerInfo.class).get();
        getGameWorld().addEntityFactory(new GameElementsFactory(playerInfo));
        gameMap = new GameMap();
        questionsPool = loadQuestions();
        doorsPool = JsonLoader.loadJSON(mainMenu.getPathToConfigurationFolder() + "doors.json", DoorsPool.class);
        itemsPool = JsonLoader.loadJSON(mainMenu.getPathToConfigurationFolder() + "items.json", ItemsPool.class);
        npcsPool = JsonLoader.loadJSON(mainMenu.getPathToConfigurationFolder() + "npcs.json", NpcsPool.class);
        levelsManager = JsonLoader.loadJSON(mainMenu.getPathToConfigurationFolder() + "levels.json", LevelsManager.class);
        levelsManager.get().setGameMap(gameMap);
        getWorldProperties().setValue("questions", questionsPool);
        getWorldProperties().setValue("levels", levelsManager.get());
        getWorldProperties().setValue("items", itemsPool.get());
        getWorldProperties().setValue("npcs", npcsPool.get());
        getWorldProperties().setValue("doors", doorsPool.get());
        Level level = levelsManager.get().getLevel();
        playerComponent = player.getComponent(PlayerComponent.class);

        getGameWorld().setLevel(level);
        getEventBus().addEventHandler(CHANGE, new RoomChangedHandler(playerComponent));
        getEventBus().fireEvent(new RoomChangeEvent(CHANGE, level));
        startTime = Instant.now();
        getGameScene().getViewport().bindToEntity(player, (float)getAppWidth()/2 - 48, (float)getAppHeight()/2 - 48);
        getGameScene().getViewport().setBounds(0, 0, 48*125, 48*125);
        getGameScene().getViewport().setLazy(true);
        getGameScene().getViewport().setZoom(1.5);
        MusicManager.playGame();
    }

    private QuestionsPool loadQuestions() {
        QuestionsPoolBuilder builder = new QuestionsPoolBuilder(getAssetLoader());
        builder.loadQuestions(mainMenu.getPathToConfigurationFolder() + "multipleChoiceQuestions.json", MultipleChoiceQuestion[].class);
        builder.loadQuestions(mainMenu.getPathToConfigurationFolder() + "singleChoiceQuestions.json", SingleChoiceQuestion[].class);
        builder.shuffle();
        QuestionsPool pool = builder.getResult();
        if (pool.getQuestionsNumber() == 0)
            throw new RuntimeException("Couldn't load any question");
        return pool;
    }

    /***
     * describes what should happen when collision occurs between entities
     */

    @Override
    protected void initPhysics() {
        BlockCollisionHandler blockCollisionHandler = new BlockCollisionHandler(PLAYER, WALL, playerComponent);
        getPhysicsWorld().addCollisionHandler(blockCollisionHandler);
        getPhysicsWorld().addCollisionHandler(blockCollisionHandler.copyFor(PLAYER, NPC));
        getPhysicsWorld().addCollisionHandler(new DoorCollisionHandler(PLAYER, DOOR, gameMap));
    }

    @Override
    protected void initUI() {
        HealthPointsBar healthPoints = new HealthPointsBar();
        healthPoints.setTranslateX(50); // set x position
        healthPoints.setTranslateY(getGameScene().getAppHeight() - healthPoints.getHeight() - 40); // set y position
        IntegerProperty playerHealthProperty = player.getComponent(HealthComponent.class).healthPointsProperty();
        healthPoints.currentHealthPointsProperty().bind(playerHealthProperty);

        getGameScene().addUINode(healthPoints); // add to the scene graph

        EqVBox eqBox = new EqVBox(player.getComponent(EqComponent.class));
        getGameScene().addUINode(eqBox);
        eqBox.setTranslateX((getAppWidth() - eqBox.getPrefWidth()) / 2);
        eqBox.setTranslateY((getAppHeight() - eqBox.getPrefHeight()) / 2);

        getGameScene().addUINode(questsView);

        resetExpBar();
        addEndGameListeners();
        FXGLDefaultMenu.MenuContent menuContent = new FXGLDefaultMenu.MenuContent();
        menuContent.getChildren().add(getUIFactoryService().newText("Find gold raspberry"));
        menuContent.visibleProperty().setValue(true);
    }

    private void addEndGameListeners() {
        EqComponent eqComponent = player.getComponent(EqComponent.class);
        HealthComponent health = player.getComponent(HealthComponent.class);
        ExpComponent exp = player.getComponent(ExpComponent.class);

        health.healthPointsProperty().addListener((v, oldValue, newValue) -> {
            if (newValue.equals(0)) {
                levelsManager.get().finishLevelWithScore(2.0F);
                nextLevel(exp, eqComponent, health, false);
            }
        });

        levelsManager.get().getLevelGaveUpProperty().addListener((v, oldValue, newValue) -> {
            if (newValue) {
                levelsManager.get().finishLevelWithScore(2.0F);
                nextLevel(exp, eqComponent, health, false);
            }
        });

        eqComponent.containsCupItemProperty().addListener((v, oldValue, newValue) -> {
            if (newValue) {
                CupItem cupItem = eqComponent.getLastItemOfType(CupItem.class);
                levelsManager.get().finishLevelWithScore(cupItem.awardTheScore());
                nextLevel(exp, eqComponent, health, true);
            }
        });
    }

    private void nextLevel(ExpComponent exp, EqComponent eqComponent, HealthComponent health, boolean passingLevelResult) {
        Level level = levelsManager.get().tryGetNextLevel(passingLevelResult);
        if (level == null) {
            String message = levelsManager.get().allLevelsFinished() ? "Game finished!" : "You lose...";
            new GameOverDialog(
                    message,
                    levelsManager.get().getScoresForLevels(),
                    startTime,
                    player.getComponent(StatisticsComponent.class)
            ).show();
        } else {
            exp.reset(levelsManager.get().getMaxExpForLevel());
            levelsManager.get().setUpForCurrentLevel(eqComponent, health);
            resetExpBar();
            level.getEntities().stream()
                    .filter(e -> e.isType(PLAYER))
                    .findFirst()
                    .ifPresent(p -> {
                        player.setPosition(p.getPosition());
                        p.setVisible(false);
                    });
            getGameWorld().setLevel(level);
            getEventBus().fireEvent(new RoomChangeEvent(CHANGE, level));
        }
    }

    private void gameOver(String message, Integer score) {
        getDialogService().showConfirmationBox(message + "\nYou finished with total score " + score.toString() + "\nRestart?", yes -> {
            if (yes)
                getGameController().startNewGame();
            else
                getGameController().gotoMainMenu();
        });
    }

    private void resetExpBar() {
        if (expBar != null) {
            getGameScene().removeUINode(expBar);
        }

        expBar = new ProgressBar();
        int barWidth = 200;
        int barHeight = 15;
        expBar.setWidth(barWidth);
        expBar.setHeight(barHeight);
        expBar.setTranslateX(getGameScene().getAppWidth() / 2 - barWidth / 2); // set x position
        expBar.setTranslateY(getGameScene().getAppHeight() - barHeight - 20); // set y position
        expBar.setBackgroundFill(Color.valueOf("f0eded"));
        expBar.setFill(Color.valueOf("58d680"));
        expBar.setTraceFill(Color.valueOf("58d680"));
        expBar.setLabelVisible(true);
        expBar.setLabelFill(Color.WHITE);
        expBar.setMaxValue(player.getComponent(ExpComponent.class).maxExpPoints);
        expBar.currentValueProperty().bind(player.getComponent(ExpComponent.class).expPointsProperty());

        getGameScene().addUINode(expBar);
    }

    public static Entity getPlayer() {
        return player;
    }

    public static void setPlayer(Entity e) {
        player = e;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
