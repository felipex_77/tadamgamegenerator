package com;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

import static com.almasb.fxgl.dsl.FXGL.getDialogService;
import static com.almasb.fxgl.dsl.FXGL.getUIFactoryService;

public class Tutorial {

    public static void startTutorial() {
        getDialogService().showBox(
                "TUTORIAL \n",
                getContent(),
                new Button("OK"));
    }

    public static VBox getContent() {
        return new VBox(
                getUIFactoryService().newText("Your are a student "),
                getUIFactoryService().newText("Your task is to collect ECTS points to pass a semester"),
                getUIFactoryService().newText("To collect ECTS you will have to find NPC and answer their question"),
                getUIFactoryService().newText("But be careful, when you give many wrong answers your will failed.")
        );
    }
}
