package com.collisionHandlers;

import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.physics.CollisionHandler;
import com.components.PlayerComponent;

public class BlockCollisionHandler extends CollisionHandler {
    private final PlayerComponent playerComponent;

    public BlockCollisionHandler(Object a, Object b, PlayerComponent playerComponent) {
        super(a, b);
        this.playerComponent = playerComponent;
    }

    @Override
    protected void onCollisionBegin(Entity player, Entity wall) {
        playerComponent.startCollision();
    }

    @Override
    protected void onCollision(Entity player, Entity wall) {
        playerComponent.solveCollision();
    }

    @Override
    protected void onCollisionEnd(Entity a, Entity b) {
        playerComponent.endCollision();
    }
}
