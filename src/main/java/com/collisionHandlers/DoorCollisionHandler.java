package com.collisionHandlers;

import com.GameMap;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.level.Level;
import com.almasb.fxgl.physics.CollisionHandler;
import com.components.DoorComponent;
import com.events.RoomChangeEvent;
import javafx.geometry.Point2D;

import static com.almasb.fxgl.dsl.FXGL.getEventBus;
import static com.almasb.fxgl.dsl.FXGL.getGameWorld;
import static com.data.EntityType.DOOR;
import static com.events.RoomChangeEvent.CHANGE;

public class DoorCollisionHandler extends CollisionHandler {

    private final GameMap gameMap;

    public DoorCollisionHandler(Object a, Object b, GameMap gameMap) {
        super(a, b);
        this.gameMap = gameMap;
    }

    @Override
    protected void onCollisionBegin(Entity player, Entity door) {
        DoorComponent doorComponent = door.getComponent(DoorComponent.class);
        if (doorComponent.canEnter()) {
            Level enteredRoom = gameMap.loadRoom(doorComponent.getEnteredRoomName());
            enteredRoom.getEntities().stream().filter(e -> e.isType(DOOR))
                    .map(e -> e.getComponent(DoorComponent.class))
                    .filter(d -> d.getDirection().equals(doorComponent.getDirection().opposite()))
                    .findFirst()
                    .ifPresent(d -> {
                        Point2D offset = new Point2D(0, 0);
                        Entity doorEntity = d.getEntity();
                        switch (d.getDirection()) {
                            case NORTH:
                                offset = new Point2D(0, doorEntity.getHeight());
                                break;
                            case WEST:
                                offset = new Point2D(doorEntity.getWidth(), 0);
                                break;
                            case SOUTH:
                                offset = new Point2D(0, -doorEntity.getHeight());
                                break;
                            case EAST:
                                offset = new Point2D(-doorEntity.getWidth(), 0);
                                break;
                        }
                        player.setPosition(doorEntity.getPosition().add(offset));
                    });
            getGameWorld().setLevel(enteredRoom);
            getEventBus().fireEvent(new RoomChangeEvent(CHANGE, enteredRoom));
        }
    }
}
