package com.components;

import com.almasb.fxgl.entity.component.Component;
import com.components.credits.Credit;
import com.data.Direction;
import com.interactive.Interactive;
import com.interactive.NotificationInteractive;

/***
 * contains data needed to load appropriate room after collision with door's graphical representation
 */
public class DoorComponent extends Component {
    private static final String roomSuffix = ".tmx";
    private final String enteredRoomName;
    private final Direction direction;
    private final Credit condition;
    private final Interactive notPassInteractive;

    public DoorComponent(String enteredRoomName, Direction direction, Credit condition) {
        this.enteredRoomName = enteredRoomName + roomSuffix;
        this.direction = direction;
        this.condition = condition;
        this.notPassInteractive = new NotificationInteractive(condition);
    }

    public String getEnteredRoomName() {
        return enteredRoomName;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean canEnter() {
        boolean canEnter = this.condition.checkCredits();
        if (!canEnter) {
            notPassInteractive.interact();
        }
        return canEnter;
    }
}
