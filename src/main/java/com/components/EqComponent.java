package com.components;

import com.almasb.fxgl.entity.component.Component;
import com.components.items.CupItem;
import com.components.items.Item;
import com.components.items.ItemFactory;
import com.ui.EqVBox;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class EqComponent extends Component {
    private final List<Item> itemList;
    private final int eqSize;

    private final BooleanProperty hasCupItemProperty = new SimpleBooleanProperty(false);

    private EqVBox box;

    public EqComponent(int eqSize) {
        assert eqSize > 0: "Size of eq must be positive";
        this.eqSize = eqSize;
        itemList = new ArrayList<>(eqSize);
    }

    public boolean canAddItem() {
        return itemList.size() <= eqSize;
    }

    public void addItem(Item item) {
        assert canAddItem(): "Trying add item to full eq";
        itemList.add(item);
        box.updateItems();

        if (item instanceof CupItem) {
            hasCupItemProperty.setValue(true);
        }
    }

    public void reset(boolean removeAll) {
        if (removeAll) {
            itemList.clear();
        } else {
            itemList.removeIf(item -> item instanceof CupItem);
        }
        hasCupItemProperty.setValue(false);
    }

    public List<Item> getItems() {
        return itemList;
    }

    public int getEqSize() {
        return eqSize;
    }

    public void useItem(Item item) {
        assert itemList.contains(item): "Item not in eq";
        if (item.itemLastUse()) {
            removeItem(item);
        }
        item.use();
    }

    public void insertItemsToMenu(List<String> rewardItemsNames) {
        rewardItemsNames.stream()
                .map(ItemFactory::buildItem)
                .forEach(this::addItem);
    }

    public void removeItemsFromMenu(List<String> requiredItemsNames) {
        List<Item> items = getItems();
        for(String itemName: requiredItemsNames) {
            for (Item item: items) {
                if (item.getName().equals(itemName)) {
                    items.remove(item);
                    break;
                }
            }
        }
    }

    public boolean allItemsAreInTheMenu(List<String> requiredItemsNames) {
        List<String> items = getItems().stream().map(Item::getName).collect(Collectors.toList());
        return items.containsAll(requiredItemsNames);
    }


    public void removeItem(Item item) {
        itemList.remove(item);
    }

    public void addEqVBox(EqVBox box) {
        this.box = box;
    }

    public void displayMenu() {
        box.changeVisibility();
    }

    public BooleanProperty containsCupItemProperty() {
        return hasCupItemProperty;
    }

    public <T extends Item> T getLastItemOfType(Class<T> type) {
        try {
            return (T) itemList.stream().filter(item -> item.getClass() == type).findFirst().get();
        } catch (Exception e) {
            return null;
        }
    }
}
