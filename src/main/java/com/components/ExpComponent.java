package com.components;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.component.Component;
import com.configuration.MusicManager;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ExpComponent extends Component {
    private final IntegerProperty expPoints;
    public int maxExpPoints;

    public ExpComponent(int maxExpPoints){
        expPoints = new SimpleIntegerProperty(0);
        this.maxExpPoints = maxExpPoints;
    }

    public void reset(int maxExpPoints) {
        this.maxExpPoints = maxExpPoints;
        expPoints.set(0);
    }

    public void increase(){
        increase(1);
    }

    public void increase(int points){
        expPoints.set(Math.min(maxExpPoints, expPoints.get()+points));
        MusicManager.playExpSound();
    }

    public IntegerProperty expPointsProperty() {
        return expPoints;
    }

    public boolean isMaximized() {
        return expPoints.getValue() == maxExpPoints;
    }
}

