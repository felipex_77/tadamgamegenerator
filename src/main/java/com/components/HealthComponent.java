package com.components;

import com.almasb.fxgl.entity.component.Component;
import com.configuration.MusicManager;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class HealthComponent extends Component {
    private final IntegerProperty healthPoints;
    private final int maxHealthPoints;

    public HealthComponent(int maxHealthPoints){
        this.maxHealthPoints = maxHealthPoints;
        healthPoints = new SimpleIntegerProperty(maxHealthPoints);
    }

    public void reset() {
        healthPoints.set(maxHealthPoints);
    }

    public void increase(){
        increase(1);
    }

    public void decrease(){
        decrease(1);
    }

    public void increase(int points){
        healthPoints.set(Math.min(maxHealthPoints, healthPoints.get()+points));
        MusicManager.playNewHealthSound();
    }

    public void decrease(int points){
        healthPoints.set(Math.max(0, healthPoints.get()-points));
    }

    public IntegerProperty healthPointsProperty() {
        return healthPoints;
    }
}
