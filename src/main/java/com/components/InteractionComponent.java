package com.components;

import com.almasb.fxgl.entity.component.Component;
import com.components.credits.Credit;
import com.interactive.Interactive;
import com.interactive.NotificationInteractive;

/**
 * Component that is responsible for proper delegation of interactions and
 * highlighting possibility of an interaction.
 *
 */
public class InteractionComponent extends Component {

    private final Interactive targetHandler;
    private final Interactive defaultHandler;
    private final Credit condition;
    private final double OPACITY_LOWER_BOUND = 0.25;
    private final double OPACITY_UPPER_BOUND = 1.0;
    private double opacityDelta = -0.01;
    private boolean isActive = false;

    public InteractionComponent(Interactive handler, Credit condition) {
        this.condition = condition;
        this.targetHandler = handler;
        this.defaultHandler = new NotificationInteractive(condition);
    }

    @Override
    public void onUpdate(double tpf) {
        if(isActive){
            double opacity = getEntity().getOpacity();
            if(opacity < OPACITY_LOWER_BOUND || opacity > OPACITY_UPPER_BOUND){
                opacityDelta = -opacityDelta;
            }
            getEntity().setOpacity(opacity + opacityDelta);
        }
    }

    public void interact() {
        Interactive currentHandler = condition.checkCredits() ? targetHandler : defaultHandler;
        currentHandler.interact();
        if (currentHandler.isLastInteraction()) {
            getEntity().setReusable(false);
            getEntity().removeFromWorld();
        }
    }

    public void setActive(boolean isActive){
        if(!isActive) {
            getEntity().setOpacity(1.0);
            opacityDelta = - Math.abs(opacityDelta);
        }
        this.isActive = isActive;
    }
}
