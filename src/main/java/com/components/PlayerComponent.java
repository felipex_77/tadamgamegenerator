package com.components;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.component.Component;
import com.almasb.fxgl.entity.components.TransformComponent;
import com.almasb.fxgl.notification.NotificationService;
import com.model.ItemInfo;
import javafx.geometry.Point2D;

import java.util.*;


public class PlayerComponent extends Component {
    // contains all interactive entities in the current room
    private List<Entity> interactiveEntities = new LinkedList<>();
    private Optional<Entity> activeEntity = Optional.empty(); // entity that player can interact with (has InteractionComponent)
    private final static double INTERACTION_THRESHOLD = 70; // distance boundary to make interaction
    private final static double SPEED = 150; // could be large, tpf is small
    private Point2D vector = new Point2D(0, 0);
    private final TransformComponent position = new TransformComponent();
    private Point2D negativePositionDiff = position.getPosition();
    private final List<ItemInfo> requiredQuestItems = new LinkedList<>();

    private boolean isCollision = false;
    private boolean toNotify = false;
    private boolean toSimpleNotify = true;

    private final NotificationService service = FXGL.getNotificationService();

    @Override
    public void onUpdate(double tpf) {
        notifyIfBegin();
        updatePosition(tpf);
    }

    private void updatePosition(double timeSinceLastUpdate) {
        double factor = SPEED * timeSinceLastUpdate / Math.max(vectorLength(), 1);
        updateInteractions();
        if (vectorLength() > 0) {
            if (!isCollision) position.translate(vector.multiply(factor));
        }
        negativePositionDiff = vector.multiply(-factor);
        vector = vector.multiply(0);
    }

    private void updateInteractions() {
        Optional<Entity> newActiveEntity = interactiveEntities.stream()
                .min(Comparator.comparingDouble(this::euclideanDistance))
                .filter(e -> euclideanDistance(e) <= INTERACTION_THRESHOLD)
                .filter(e -> e.hasComponent(InteractionComponent.class));


        if (!newActiveEntity.equals(activeEntity)) {
            if (activeEntity.isPresent() &&
                    activeEntity.get().hasComponent(InteractionComponent.class)) {
                activeEntity.get().getComponent(InteractionComponent.class).setActive(false);
            }
            newActiveEntity.ifPresent(e -> e.getComponent(InteractionComponent.class).setActive(true));
            activeEntity = newActiveEntity;
        }
    }


    public void updateInteractiveEntities(List<Entity> newInteractiveEntities) {
        interactiveEntities = newInteractiveEntities;
    }

    private double euclideanDistance(Entity e) {
        double x = position.getX();
        double y = position.getY();
        return Math.sqrt(Math.pow(x - e.getX(), 2) + Math.pow(y - e.getY(), 2));
    }

    public void up() {
        vector = vector.add(0, -1);
    }

    public void down() {
        vector = vector.add(0, 1);
    }

    public void left() {
        vector = vector.add(-1, 0);
    }

    public void right() {
        vector = vector.add(1, 0);
    }

    private double vectorLength() {
        return Math.sqrt(vector.dotProduct(vector));
    }

    public void startCollision() {
        isCollision = true;
    }

    public void solveCollision() {
        position.translate(negativePositionDiff);
    }

    public void endCollision() {
        isCollision = false;
    }

    public void notifyIfBegin() {
        if (toNotify) {
            service.pushNotification("Press W,S,A,D to move");
            service.pushNotification("To change room go to door");
            service.pushNotification("Press SPACE near NPC to get question");
            service.pushNotification("To open EQ click E,");
            service.pushNotification("To use item in eq click mouse on it");
            service.pushNotification("Press N to see tips");
            service.pushNotification("Press F to give up level");
            toNotify = false;
        }
        if (toSimpleNotify) {
            service.pushNotification("Press N to see tips");
            toSimpleNotify = false;
        }
    }

    public void resetNotifications() {
        toNotify = true;
    }

    public void makeInteraction() {
        activeEntity.ifPresent(this::interactIfComponentPresent);
    }

    public void interactIfComponentPresent(Entity e) {
        InteractionComponent component = e.getComponent(InteractionComponent.class);
        if (component != null) {
            component.interact();
        }
    }

    public List<ItemInfo> getRequiredQuestItems() {
        return requiredQuestItems;
    }

    public void addRequiredQuestItem(ItemInfo itemInfo) {
        requiredQuestItems.add(Objects.requireNonNull(itemInfo));
    }

    public void removeRequiredQuestItem(ItemInfo itemInfo) {
        requiredQuestItems.remove(Objects.requireNonNull(itemInfo));
    }
}
