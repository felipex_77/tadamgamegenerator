package com.components;

import com.almasb.fxgl.entity.component.Component;
import com.model.QuestionDifficulty;

import java.util.HashMap;

public class StatisticsComponent extends Component {
    private HashMap<QuestionDifficulty, Integer> nOfCorrectAnswers;
    private HashMap<QuestionDifficulty, Integer> nOfAllAnswers;

    public StatisticsComponent() {
        nOfCorrectAnswers = new HashMap<>();
        nOfAllAnswers = new HashMap<>();

        for (QuestionDifficulty d : QuestionDifficulty.values()) {
            nOfCorrectAnswers.put(d, 0);
            nOfAllAnswers.put(d, 0);
        }
    }

    public void incrementCorrect(QuestionDifficulty d){
        nOfCorrectAnswers.replace(d, nOfCorrectAnswers.get(d) + 1);
    }

    public void incrementAnswers(QuestionDifficulty d){
        nOfAllAnswers.replace(d, nOfAllAnswers.get(d) + 1);
    }

    public Integer getNOfCorrectAnswers(QuestionDifficulty d){
        return nOfCorrectAnswers.get(d);
    }

    public Integer getNOfAnswers(QuestionDifficulty d){
        return nOfAllAnswers.get(d);
    }
}
