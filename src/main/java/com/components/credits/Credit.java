package com.components.credits;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Credit {
    private final Set<Credit> prerequisites = new HashSet<>();
    private final CreditCondition condition;
    private boolean isPassed = false;

    public Credit() {
        this.condition = new InstantPassCondition();
    }

    public Credit(CreditCondition condition) {
        this.condition = condition;
    }

    public boolean checkCredits() {
        if (isPassed)
            return true;
        this.update();
        return isPassed;
    }

    private void update() {
        isPassed = condition.isPassed() && this.prerequisites.stream().allMatch(Credit::checkCredits);
    }

    public void addPrerequisite(Credit prerequisite) {
        this.prerequisites.add(prerequisite);
    }

    private CreditCondition getCondition() {
        return condition;
    }

    public List<CreditCondition> getNotPassedConditions() {
        List<CreditCondition> requirementsList = new ArrayList<>();
        if (!condition.isPassed()) {
            requirementsList.add(condition);
        }
        prerequisites.stream()
                .map(Credit::getCondition)
                .filter(creditCondition -> !creditCondition.isPassed())
                .forEach(requirementsList::add);
        return requirementsList;
    }
}
