package com.components.credits;

import com.GameRunner;
import com.components.ExpComponent;

public class ExpCondition implements CreditCondition {
    private final int expThreshold;

    public ExpCondition(int expThreshold) {
        this.expThreshold = expThreshold;
    }

    public boolean isPassed() {
        ExpComponent exp = GameRunner.getPlayer().getComponent(ExpComponent.class);
        return exp.expPointsProperty().getValue() >= expThreshold;
    }

    @Override
    public String toString() {
        return expThreshold + " EXP";
    }
}
