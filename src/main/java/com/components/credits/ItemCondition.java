package com.components.credits;

import com.GameRunner;
import com.almasb.fxgl.entity.Entity;
import com.components.EqComponent;

public class ItemCondition implements CreditCondition {
    private final String requiredItemName;

    public ItemCondition(String requiredItemName) {
        this.requiredItemName = requiredItemName;
    }

    public boolean isPassed() {
        Entity player = GameRunner.getPlayer();
        EqComponent eqComponent = player.getComponent(EqComponent.class);
        return eqComponent.getItems().stream().anyMatch(item -> item.getName().equals(requiredItemName));
    }

    @Override
    public String toString() {
        return requiredItemName;
    }
}
