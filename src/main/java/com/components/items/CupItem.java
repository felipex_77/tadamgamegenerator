package com.components.items;

import com.model.ItemInfo;
import com.util.LineFormatter;

import static com.almasb.fxgl.dsl.FXGLForKtKt.getDialogService;

public class CupItem extends Item {

    private final float score;
    private boolean taken = false;

    public CupItem(ItemInfo itemInfo, float score) {
        super(itemInfo);
        this.score = score;
    }

    public float awardTheScore() {
        return score;
    }

    @Override
    public void interact() {
        getDialogService().showConfirmationBox(LineFormatter.formatLines("If You take this cup, You will get a grade "
                + score + " for this level of the game. Agree?", 50), answer -> {
            if (answer) {
                taken = true;
                super.interact();
            }
        });
    }

    @Override
    public boolean isLastInteraction() {
        return super.isLastInteraction() && taken;
    }
}
