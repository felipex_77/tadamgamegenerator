package com.components.items;


import com.GameRunner;
import com.components.ExpComponent;
import com.model.ItemInfo;

public class ExpItem extends Item {

    public ExpItem(ItemInfo itemInfo) {
        super(itemInfo);
    }

    @Override
    public void use() {
        ExpComponent expComponent = GameRunner.getPlayer().getComponent(ExpComponent.class);
        expComponent.increase();
    }
}
