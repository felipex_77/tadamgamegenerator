package com.components.items;

import com.GameRunner;
import com.components.HealthComponent;
import com.model.ItemInfo;

public class HealthPotionItem extends Item {

    public HealthPotionItem(ItemInfo itemInfo) {
        super(itemInfo);
    }

    @Override
    public void use() {
        HealthComponent component = GameRunner.getPlayer().getComponent(HealthComponent.class);
        component.increase(1);
    }
}
