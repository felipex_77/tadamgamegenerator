package com.components.items;

import com.GameRunner;
import com.almasb.fxgl.entity.Entity;
import com.components.EqComponent;
import com.interactive.Interactive;
import com.model.ItemInfo;

public class Item implements Interactive {

    protected ItemInfo info;

    public Item(ItemInfo itemInfo) {
        this.info = itemInfo;
    }

    public Item() {
        info = new ItemInfo();
    }

    /**
     *  Add item to player eq
     */
    public void interact() {
        Entity player = GameRunner.getPlayer();
        EqComponent eqComponent = player.getComponent(EqComponent.class);
        if (eqComponent.canAddItem()) {
            eqComponent.addItem(this);
        }
    }

    public String getAssetName() {
        return info.getImageAsset();
    }

    public String getName() { return info.getName();}

    public String getDescriptions() {
        return info.getDescription();
    }

    /**
     *  Actions to make when item is used
     */
    public void use() {}

    public boolean itemLastUse() {
        return info.isDisposable();
    }

    @Override
    public boolean isLastInteraction() {
        return info.isDisposable();
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
