package com.components.items;

import com.configuration.ItemsPool;
import com.model.ItemInfo;

import static com.almasb.fxgl.dsl.FXGL.getWorldProperties;

public class ItemFactory {

    private ItemFactory(){}

    public static Item buildItem(String itemName) {
        ItemsPool pool = getWorldProperties().getValue("items");
        ItemInfo itemInfo = pool.getItem(itemName).get();
        switch (itemName) {
            case "magic-book":
            case "elixir-exp":
                return generateExpItem(itemInfo);
            case "health-potion":
            case "elixir-health":
                return generateHealthItem(itemInfo);
            default:
                return generatePickOnlyItem(itemInfo);
        }
    }

    private static ExpItem generateExpItem(ItemInfo itemInfo) {
        return new ExpItem(itemInfo);
    }

    private static HealthPotionItem generateHealthItem(ItemInfo itemInfo) {
        return new HealthPotionItem(itemInfo);
    }

    private static PickOnlyItem generatePickOnlyItem(ItemInfo itemInfo) {
        return new PickOnlyItem(itemInfo);
    }
}
