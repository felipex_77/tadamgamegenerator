package com.components.items;

import com.model.ItemInfo;

public class PickOnlyItem extends Item {
    public PickOnlyItem(ItemInfo itemInfo) {
        super(itemInfo);
    }

    @Override
    public boolean itemLastUse() {
        return false;
    }
}
