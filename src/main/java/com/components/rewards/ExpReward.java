package com.components.rewards;

import com.GameRunner;
import com.components.ExpComponent;

public class ExpReward extends Reward{
    private final int expPoints;

    public ExpReward(int expPoints){
        this.expPoints = expPoints;
    }

    @Override
    public void interact() {
        ExpComponent expComponent = GameRunner.getPlayer().getComponent(ExpComponent.class);
        expComponent.increase(expPoints);
    }
}
