package com.components.rewards;

import com.interactive.Interactive;

public abstract class Reward implements Interactive {
    @Override
    public abstract void interact();

    @Override
    public boolean isLastInteraction() {
        return false;
    }
}
