package com.configuration;

import com.model.DoorInfo;

import java.util.Arrays;
import java.util.Optional;

public class DoorsPool {
    private DoorInfo[] doors;

    public DoorsPool() {
    }

    public void setDoors(DoorInfo[] doors) {
        this.doors = doors;
    }

    public Optional<DoorInfo> getDoor(String doorName){
        return Arrays.stream(doors)
            .filter(d -> d.getName().equals(doorName))
            .findFirst();
    }
}
