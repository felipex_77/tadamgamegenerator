package com.configuration;

import com.model.ItemInfo;

import java.util.List;
import java.util.Optional;

/***
 * POJO created from items.json configuration file
 * allows taking items info by destination class type
 */

public class ItemsPool {
    private List<ItemInfo> items;

    public ItemsPool(List<ItemInfo> items) {
        this.items = items;
    }

    public ItemsPool() {
    }

    public void setItems(List<ItemInfo> items) {
        this.items = items;
    }

    public Optional<ItemInfo> getItem(String name) {
        return items.stream()
                .filter(itemInfo -> itemInfo.getName().equals(name))
                .findFirst();
    }
}
