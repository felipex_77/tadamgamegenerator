package com.configuration;

import com.GameMap;
import com.almasb.fxgl.entity.level.Level;
import com.components.EqComponent;
import com.components.HealthComponent;
import com.model.CupInfo;
import com.model.LevelInfo;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import static com.almasb.fxgl.dsl.FXGLForKtKt.getDialogService;

public class LevelsManager {

    private List<LevelInfo> levelsInfo;
    private final HashMap<String, Float> scoresForLevels = new HashMap<>();

    private final BooleanProperty levelGaveUp = new SimpleBooleanProperty(false);

    private int currentLevelIndex = 0;
    private GameMap gameMap;

    public LevelsManager() {}

    public void setLevelsInfo(List<LevelInfo> levelsInfo) {
        this.levelsInfo = levelsInfo;
    }

    public void setGameMap(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    public boolean allLevelsFinished() {
        return currentLevelIndex == levelsInfo.size();
    }

    public Level tryGetNextLevel(boolean passingLevelResult) {
        if (!passingLevelResult && currentLevelInfo().isSuccessfulCompletionRequired()) {
            return null;
        }
        currentLevelIndex ++;
        if (allLevelsFinished()) {
            return null;
        }
        levelGaveUp.setValue(false);
        return getLevel();
    }

    public Level getLevel() {
        if (currentLevelInfo() != null) {
            return gameMap.loadRoom(currentLevelInfo().getFirstRoom());
        } else {
            return null;
        }
    }

    public void finishLevelWithScore(float score) {
        if (currentLevelInfo() != null) {
            scoresForLevels.put(currentLevelInfo().getFullLevelName(), score);
        }
    }

    public int getMaxExpForLevel() {
        try {
            return currentLevelInfo().getCups().stream().mapToInt(CupInfo::getExpRequired).max().getAsInt();
        } catch (NullPointerException | NoSuchElementException e) {
            return 0;
        }
    }

    public HashMap<String, Float> getScoresForLevels() {
        return scoresForLevels;
    }

    public CupInfo getCupWithNameOnCurrentLevel(String name) {
        try {
            return currentLevelInfo()
                    .getCups()
                    .stream()
                    .filter(cup -> cup.getName().equals(name))
                    .findFirst()
                    .get();
        } catch (NullPointerException | NoSuchElementException e) {
            return null;
        }
    }

    public void giveUpLevel() {
        String message = currentLevelInfo().isSuccessfulCompletionRequired()
                ? "Are you sure you want to give up this level?\n" +
                "If so, this level will be considered as failed\n" +
                "and you will lose game. Do you agree?"
                : "Are you sure you want to give up this level?\n" +
                "If so, this level will be considered as failed,\n" +
                "but you will advance to the next level. Do you agree?";
        getDialogService().showConfirmationBox(message, answer -> {
            if (answer) {
                levelGaveUp.setValue(true);
            }
        });
    }

    public BooleanProperty getLevelGaveUpProperty() {
        return levelGaveUp;
    }

    public void setUpForCurrentLevel(EqComponent eqComponent, HealthComponent health) {
        if (currentLevelInfo().isPlayerReset()) {
            eqComponent.reset(true);
            health.reset();
        } else {
            eqComponent.reset(false);
        }
    }

    private LevelInfo currentLevelInfo() {
        try {
            return levelsInfo.get(currentLevelIndex);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}
