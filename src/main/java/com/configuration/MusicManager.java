package com.configuration;

import com.almasb.fxgl.audio.Sound;
import com.almasb.fxgl.dsl.FXGL;

import static com.almasb.fxgl.dsl.FXGL.getSettings;
import static com.almasb.fxgl.dsl.FXGLForKtKt.loopBGM;

public class MusicManager {
    private static final Sound expSound = FXGL.getAssetLoader().loadSound("coinSound.wav");
    private static final Sound newHealthSound = FXGL.getAssetLoader().loadSound("coinSound.wav");
    private static final Sound wrongAnswerSound = FXGL.getAssetLoader().loadSound("wrongAnswer.wav");

    public static void playMenu() {
//        MusicManager.reset();
//        loopBGM("haloTheme.mp3");
    }

    public static void playGame() {
//        MusicManager.reset();
//        loopBGM("mixkit-sicilian-coffee-613.mp3");
    }

    public static void playNewHealthSound() {
        FXGL.getAudioPlayer().playSound(MusicManager.newHealthSound);
    }

    public static void playExpSound() {
        FXGL.getAudioPlayer().playSound(MusicManager.expSound);
    }

    public static void playWrongAnswer() {
        FXGL.getAudioPlayer().playSound(MusicManager.wrongAnswerSound);
    }

    private static void reset() {
        FXGL.getAudioPlayer().pauseAllMusic();
        FXGL.getAudioPlayer().stopAllMusic();

        getSettings().setGlobalSoundVolume(0.5);
        getSettings().setGlobalMusicVolume(0.3);
    }


}
