package com.configuration;

import com.model.npc.NpcInfo;

import java.util.List;
import java.util.Optional;

public class NpcsPool {
    private List<NpcInfo> npcs;

    public NpcsPool(List<NpcInfo> npcs) {
        this.npcs = npcs;
    }

    public NpcsPool() {
    }

    public void setNpcs(List<NpcInfo> npcs) {
        this.npcs = npcs;
    }

    public Optional<NpcInfo> getNpc(String name) {
        return npcs.stream()
                .filter(npc -> npc.name.equals(name))
                .findFirst();
    }
}