package com.configuration;

import com.model.questions.Question;
import com.model.QuestionDifficulty;

import java.util.*;
import java.util.function.Predicate;

/***
 * POJO created from questions.json configuration file
 * allows taking questions, and returning them.
 * Taking questions can be parametrized and allows filtering by
 * given tags, difficulty level, or user-defined predicate.
 */

public class QuestionsPool {
    private List<Question> questions;
    private List<Question> taken = new ArrayList<>();

    public QuestionsPool(List<Question> questions) {
        this.questions = questions;
    }

    public QuestionsPool() {}

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public int getQuestionsNumber(){
        return questions.size() + taken.size();
    }

    /*
    Takes any question from pool
     */
    public Optional<Question> take() {
        Predicate<Question> alwaysTrue = q -> true;
        return take(alwaysTrue);
    }

    /*
    Takes only questions with given tags
     */
    public Optional<Question> take(Collection<String> tags){
        Predicate<Question> containsAnyTag = q -> q.getTags().stream().anyMatch(tags::contains);
        return take(containsAnyTag);
    }

    /*
    Takes only questions with given difficulty level
     */
    public Optional<Question> take(QuestionDifficulty difficultyLevel){
        Predicate<Question> containsDifficultyLevel = q -> (q.getQuestionDifficulty() == difficultyLevel);
        return take(containsDifficultyLevel);
    }

    /*
    Takes only questions with given difficulty level and tags
     */
    public Optional<Question> take(QuestionDifficulty difficultyLevel, Collection<String> tags){
        Predicate<Question> containsAnyTag = q -> q.getTags().stream().anyMatch(tags::contains);
        Predicate<Question> containsDifficultyLevel = q -> (q.getQuestionDifficulty() == difficultyLevel);
        Predicate<Question> containsDifficultyLevelAndAnyTag = q -> (containsAnyTag.test(q) && containsDifficultyLevel.test(q));
        return take(containsDifficultyLevelAndAnyTag);
    }

    /*
    Takes questions matching given predicate
     */
    public Optional<Question> take(Predicate<Question> predicate){
        var container = questions;
        Optional<Question> findResult = findQuestionIn(container, predicate);
        if (findResult.isEmpty()) {
            container = taken;
            findResult = findQuestionIn(taken, predicate);
        }
        if (findResult.isPresent()){
            Question questionToGive = findResult.get();
            container.remove(questionToGive);
            taken.add(questionToGive);
        }
        return findResult;
    }

    /*
    Returns question to the pool -> moves it from taken to available pool
     */
    public void returnQuestion(Question question){
        if (!questions.contains(question) && !taken.contains(question))
            throw new IllegalArgumentException("This question does not belong this pool");
        if (!questions.contains(question)) {
            taken.remove(question);
            questions.add(question);
        }
    }

    private Optional<Question> findQuestionIn(Collection<Question> container, Predicate<Question> predicate){
        return container.stream().filter(predicate).findFirst();
    }
}
