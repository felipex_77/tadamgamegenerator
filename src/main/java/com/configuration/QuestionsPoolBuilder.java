package com.configuration;

import com.almasb.fxgl.app.services.FXGLAssetLoaderService;
import com.model.questions.Question;
import com.util.JsonLoader;

import java.util.*;

public class QuestionsPoolBuilder {
    private final FXGLAssetLoaderService assetLoader;
    private List<Question> allQuestions = new ArrayList<>();

    public QuestionsPoolBuilder(FXGLAssetLoaderService assetLoader){
        this.assetLoader = assetLoader;
    }

    public void loadQuestions(String path, Class<? extends Question[]> questionClass){
        Optional<? extends Question[]> questions = JsonLoader.loadJSON(path, questionClass);
        questions.ifPresent(q -> allQuestions.addAll(Arrays.asList(q)));
    }

    public void shuffle(){
        Collections.shuffle(allQuestions);
    }

    public QuestionsPool getResult(){
        return new QuestionsPool(allQuestions);
    }

    public void reset(){
        allQuestions = new ArrayList<>();
    }

}
