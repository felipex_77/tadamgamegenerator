package com.data;

public enum Direction {
    NORTH, WEST, SOUTH, EAST;

    public Direction opposite(){
        switch (this){
            case NORTH: return SOUTH;
            case WEST: return EAST;
            case SOUTH: return NORTH;
            case EAST: return WEST;
            default: return null;
        }
    }
}
