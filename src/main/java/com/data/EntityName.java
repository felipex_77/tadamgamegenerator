package com.data;

public class EntityName {
    // TODO : below names also should be configurable to the user
    public static final String ENTITY_EXP = "exp";
    public static final String ENTITY_HEALTH = "health";
    public static final String ENTITY_PICK_ITEM = "pick";
    public static final String ENTITY_CUP = "cup";
    public static final String ENTITY_NPC = "npc";
    public static final String ENTITY_PLAYER = "player";
    public static final String ENTITY_WALL = "wall";
    public static final String ENTITY_DOOR = "door";

    private EntityName() {
    }
}
