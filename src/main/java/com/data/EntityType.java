package com.data;

public enum EntityType {
    WALL, DOOR, PLAYER, NPC, ITEM;
}
