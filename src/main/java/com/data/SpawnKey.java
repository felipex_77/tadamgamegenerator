package com.data;

public class SpawnKey {
    public static final String ENTERED_ROOM = "enteredRoom";
    public static final String DOOR_DIRECTION = "direction";
    public static final String PLAYER_MAX_HEALTH = "maxHealthPoints";
    public static final String PLAYER_MAX_EXP = "maxExpPoints";
    public static final String INTERACTIVE = "interactive";

    private SpawnKey(){}
}
