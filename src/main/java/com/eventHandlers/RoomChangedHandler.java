package com.eventHandlers;

import com.components.PlayerComponent;
import com.events.RoomChangeEvent;
import javafx.event.EventHandler;

import java.util.stream.Collectors;

import static com.data.SpawnKey.INTERACTIVE;

public class RoomChangedHandler implements EventHandler<RoomChangeEvent> {
    private final PlayerComponent playerComponent;

    public RoomChangedHandler(PlayerComponent playerComponent) {
        this.playerComponent = playerComponent;
    }

    @Override
    public void handle(RoomChangeEvent event) {
        playerComponent.updateInteractiveEntities(
                event.getEnteredRoom().getEntities().stream()
                        .filter(e -> e.getPropertyOptional(INTERACTIVE).isPresent())
                        .collect(Collectors.toList())
        );
    }
}
