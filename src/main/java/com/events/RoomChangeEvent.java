package com.events;

import com.almasb.fxgl.entity.level.Level;
import javafx.event.Event;
import javafx.event.EventType;

public class RoomChangeEvent extends Event {

    public static final EventType<RoomChangeEvent> CHANGE
            = new EventType<>(Event.ANY, "ROOM_CHANGE");

    private final Level enteredRoom;

    public RoomChangeEvent(EventType<? extends Event> eventType, Level enteredRoom) {
        super(eventType);
        this.enteredRoom = enteredRoom;
    }

    public Level getEnteredRoom() {
        return enteredRoom;
    }
}
