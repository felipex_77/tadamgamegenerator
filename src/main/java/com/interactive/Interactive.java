package com.interactive;

/**
 * Interface used to mark any components that player can interact with
 */
public interface Interactive {
    void interact();

    boolean isLastInteraction();
}
