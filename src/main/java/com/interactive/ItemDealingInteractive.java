package com.interactive;

import com.almasb.fxgl.cutscene.dialogue.DialogueGraph;
import com.almasb.fxgl.cutscene.dialogue.FunctionCallHandler;
import com.components.EqComponent;
import com.ui.OfferBox;
import java.util.List;

import static com.almasb.fxgl.dsl.FXGL.*;

public class ItemDealingInteractive implements Interactive {

    private final EqComponent eqComponent;
    private final List<String> requiredItemsNames;
    private final List<String> rewardItemsNames;
    private final DialogueGraph dialogueGraph;
    private final OfferBox box;

    public ItemDealingInteractive(EqComponent eqComponent, List<String> requiredItemsNames,
                                  List<String> rewardItemsNames, DialogueGraph dialogueGraph) {
        this.eqComponent = eqComponent;
        this.requiredItemsNames = requiredItemsNames;
        this.rewardItemsNames = rewardItemsNames;
        this.dialogueGraph = dialogueGraph;
        box = createBox();
    }

    @Override
    public void interact() {
        this.displayOffer();
        //map dialogue functions
        FunctionCallHandler handler = (functionName, args) -> {
            switch (functionName) {
                case "dealWithItems":
                    dealWithItems();
                    break;
                case "hasRequiredItems":
                    return eqComponent.allItemsAreInTheMenu(requiredItemsNames);
                default:
                    throw new UnsupportedOperationException(functionName + " is not supported");
            }
            return null;
        };

        getCutsceneService().startDialogueScene(dialogueGraph, handler, this::hideOffer);
    }

    public void dealWithItems() {
        eqComponent.removeItemsFromMenu(requiredItemsNames);
        eqComponent.insertItemsToMenu(rewardItemsNames);
    }

    @Override
    public boolean isLastInteraction() {
        return false;
    }

    private void displayOffer() {
                box.setVisible(true);
    }

    private void hideOffer() {
        box.setVisible(false);
    }

    private OfferBox createBox() {
        OfferBox box = new OfferBox(requiredItemsNames, rewardItemsNames);
        getGameScene().addUINode(box);
        box.setVisible(false);
        return box;
    }
}
