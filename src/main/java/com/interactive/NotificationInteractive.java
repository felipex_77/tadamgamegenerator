package com.interactive;

import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.notification.NotificationService;
import com.components.credits.Credit;
import com.components.credits.CreditCondition;

import java.util.List;

public class NotificationInteractive implements Interactive {

    private final static String BASE_MESSAGE = "You need: ";
    private final Credit credit;
    private long lastInteractTime = 0; // time prevents from too many notifications
    private final NotificationService service = FXGL.getNotificationService();

    public NotificationInteractive(Credit credit) {
        this.credit = credit;
    }

    @Override
    public void interact() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastInteractTime > 2000) {
            lastInteractTime = currentTime;
            passNotification();
        }
    }

    private void passNotification(){
        List<CreditCondition> conditions = credit.getNotPassedConditions();
        conditions.forEach(s -> service.pushNotification(BASE_MESSAGE + s.toString()));
    }

    @Override
    public boolean isLastInteraction() {
        return false;
    }
}
