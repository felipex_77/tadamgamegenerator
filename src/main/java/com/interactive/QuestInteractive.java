package com.interactive;

import com.almasb.fxgl.cutscene.dialogue.DialogueGraph;
import com.almasb.fxgl.cutscene.dialogue.FunctionCallHandler;
import com.components.EqComponent;
import com.components.PlayerComponent;
import com.configuration.ItemsPool;
import com.model.ItemInfo;

import java.util.List;

import static com.almasb.fxgl.dsl.FXGL.getCutsceneService;
import static com.almasb.fxgl.dsl.FXGL.getWorldProperties;

public class QuestInteractive implements Interactive {

    private final EqComponent eqComponent;
    private final PlayerComponent playerComponent;
    private final List<String> requiredItemsNames;
    private final List<String> rewardItemsNames;
    private final DialogueGraph dialogueGraph;

    private boolean questTaken = false;
    private boolean questFinish = false;

    public QuestInteractive(EqComponent eqComponent, PlayerComponent playerComponent, List<String> requiredItemsNames,
                            List<String> rewardItemsNames, DialogueGraph dialogueGraph) {
        this.eqComponent = eqComponent;
        this.playerComponent = playerComponent;
        this.requiredItemsNames = requiredItemsNames;
        this.rewardItemsNames = rewardItemsNames;
        this.dialogueGraph = dialogueGraph;
    }

    @Override
    public void interact() {
        FunctionCallHandler handler = (functionName, args) -> {
            switch (functionName) {
                case "questTaken":
                    return questTaken;
                case "questFinished":
                    return questFinish;
                case "conditionsFilled":
                    return eqComponent.allItemsAreInTheMenu(requiredItemsNames);
                case "rewardPlayer":
                    finishQuest();
                    break;
                case "takeQuest":
                    takeQuest();
                    break;
                default:
                    throw new UnsupportedOperationException(functionName + " is not supported");
            }
            return null;
        };
        getWorldProperties().setValue("dialogItems", buildDialogItemsString());
        getCutsceneService().startDialogueScene(dialogueGraph, handler);
    }

    private String buildDialogItemsString() {
        var stringBuilder = new StringBuilder();
        requiredItemsNames.subList(0, requiredItemsNames.size() - 1)
                .stream().map(item -> item += ", ")
                .forEach(stringBuilder::append);
        stringBuilder.append(requiredItemsNames.get(requiredItemsNames.size() - 1));
        return stringBuilder.toString();
    }

    public void finishQuest() {
        questFinish = true;
        ItemsPool pool = getWorldProperties().getValue("items");
        requiredItemsNames.forEach(name -> this.removeQuestItem(pool, name));
        rewardPlayer();
    }

    public void rewardPlayer() {
        eqComponent.removeItemsFromMenu(requiredItemsNames);
        eqComponent.insertItemsToMenu(rewardItemsNames);
    }

    public void takeQuest() {
        ItemsPool pool = getWorldProperties().getValue("items");
        requiredItemsNames.forEach(name -> this.addQuestItem(pool, name));
        questTaken = true;
    }

    private void addQuestItem(ItemsPool pool, String itemName) {
        ItemInfo itemInfo = pool.getItem(itemName).get();
        playerComponent.addRequiredQuestItem(itemInfo);
    }

    private void removeQuestItem(ItemsPool pool, String itemName) {
        ItemInfo itemInfo = pool.getItem(itemName).get();
        playerComponent.removeRequiredQuestItem(itemInfo);
    }

    @Override
    public boolean isLastInteraction() {
        return false;
    }

}
