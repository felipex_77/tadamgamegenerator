package com.interactive;

import com.GameRunner;
import com.almasb.fxgl.cutscene.dialogue.DialogueGraph;
import com.almasb.fxgl.cutscene.dialogue.FunctionCallHandler;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.components.HealthComponent;
import com.components.StatisticsComponent;
import com.components.rewards.Reward;
import com.configuration.MusicManager;
import com.configuration.QuestionsPool;
import com.model.QuestionDifficulty;
import com.model.questions.Question;
import com.model.questions.QuestionInteraction;
import com.model.questions.QuestionInteractionFactory;
import com.ui.OfferBox;
import com.ui.QuestionDialog;

import java.util.List;
import java.util.Optional;

import static com.almasb.fxgl.dsl.FXGL.getCutsceneService;
import static com.almasb.fxgl.dsl.FXGL.getWorldProperties;

/***
 * contains question interaction module, which allows communication with question
 */

public class QuestionInteractive implements Interactive {
    private Reward reward;
    private List<String> acceptedQuestionTags;
    private QuestionDifficulty acceptedDifficulty;
    private int questionsLimit;
    private boolean isFirstInteraction = true;
    private double answerResult = -1.;
    private QuestionInteraction interaction;
    private DialogueGraph dialogueGraph;
    private final QuestionsPool questionsPool = FXGL.getWorldProperties().getObject("questions");
    private final QuestionInteractionFactory questionInteractionFactory = new QuestionInteractionFactory();
    private boolean isLastInteraction;
    private String npcName;

    @Override
    public void interact() {
        //map dialogue functions
        FunctionCallHandler handler = (functionName, args) -> {
            switch (functionName) {
                case "displayQuestion":
                    displayQuestion();
                    break;
                case "answeredCorrectly":
                    return answeredCorrectly();
                case "hasQuestion":
                    return hasQuestion();
                case "isFirstInteraction":
                    return isFirstInteraction();
                case "applyQuestionResult":
                    applyQuestionResult();
                    break;
                case "closeQuestionInteraction":
                    closeQuestionInteraction();
                    break;
                default:
                    throw new UnsupportedOperationException(functionName + " is not supported");
            }
            return null;
        };
        interaction = takeQuestionInteraction();
        getWorldProperties().setValue("npcName", npcName);
        getCutsceneService().startDialogueScene(dialogueGraph, handler, () -> isFirstInteraction = false);
    }

    private void displayQuestion() {
        QuestionDialog questionDialog = new QuestionDialog(interaction, this);
        questionDialog.show();
    }

    public void answeringFinished() {
        if (interaction.isSelected()) {
            answerResult = interaction.evaluate();
            if (questionsLimit > 0)
                questionsLimit--;
        }
    }

    private void closeQuestionInteraction(){
        returnQuestion();
        interaction = null;
    }

    public void setLastInteraction(boolean isLastInteraction) {
        this.isLastInteraction = isLastInteraction;
    }

    private boolean answeredCorrectly() {
        return answerResult == 1.0;
    }

    private QuestionInteraction takeQuestionInteraction() {
        if (interaction == null && questionsLimit > 0) {
            Optional<Question> newQuestion = getNewQuestion();
            assert newQuestion.isPresent() : "No question available";
            return questionInteractionFactory.getStandardInteractionForQuestion(newQuestion.get());
        }
        return interaction;
    }

    private Optional<Question> getNewQuestion() {
        if (acceptedDifficulty != null && acceptedQuestionTags != null)
            return questionsPool.take(acceptedDifficulty, acceptedQuestionTags);
        else if (acceptedQuestionTags != null)
            return questionsPool.take(acceptedQuestionTags);
        else if (acceptedDifficulty != null)
            return questionsPool.take(acceptedDifficulty);
        else
            return questionsPool.take();
    }

    private void applyQuestionResult() {
        Entity player = GameRunner.getPlayer();
        StatisticsComponent stats = player.getComponent(StatisticsComponent.class);
        stats.incrementAnswers(acceptedDifficulty);
        if (answeredCorrectly() && reward != null) {
            // good answer
            reward.interact();
            stats.incrementCorrect(acceptedDifficulty);
        }
        else {
            MusicManager.playWrongAnswer();
            player.getComponent(HealthComponent.class).decrease();
        }
    }

    private void returnQuestion() {
        assert interaction != null : "Current question interaction is null";
        Question question = interaction.getQuestion();
        questionsPool.returnQuestion(question);
    }

    @Override
    public boolean isLastInteraction() {
        return !hasQuestion() && isLastInteraction;
    }

    public boolean isFirstInteraction() {
        return isFirstInteraction;
    }

    public boolean hasQuestion() {
        return interaction != null;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public void setAcceptedQuestionTags(List<String> acceptedQuestionTags) {
        this.acceptedQuestionTags = acceptedQuestionTags;
    }

    public void setAcceptedDifficulty(QuestionDifficulty acceptedDifficulty) {
        this.acceptedDifficulty = acceptedDifficulty;
    }

    public void setQuestionsLimit(int questionsLimit) {
        assert questionsLimit > 0 : "Questions limit must be at least 1";
        this.questionsLimit = questionsLimit;
    }

    public void setDialogueGraph(DialogueGraph dialogueGraph) {
        this.dialogueGraph = dialogueGraph;
    }

    public void setNpcName(String npcName) {
        this.npcName = npcName;
    }
}
