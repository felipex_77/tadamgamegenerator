package com.model;

import java.util.Objects;

/***
 * POJO class, represents single answer
 */

public class Answer {
    private String text;

    public Answer(String text){
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText(){
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return Objects.equals(text, answer.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text);
    }
}
