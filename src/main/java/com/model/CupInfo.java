package com.model;

public class CupInfo {

    private String name;
    private int expRequired;
    private float scoreForLevel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getScoreForLevel() {
        return scoreForLevel;
    }

    public void setScoreForLevel(float scoreForLevel) {
        this.scoreForLevel = scoreForLevel;
    }

    public int getExpRequired() {
        return expRequired;
    }

    public void setExpRequired(int expRequired) {
        this.expRequired = expRequired;
    }
}
