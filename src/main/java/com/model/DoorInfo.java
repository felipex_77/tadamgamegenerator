package com.model;

import com.data.Direction;

public class DoorInfo {
    private String name;
    private String enteredRoom;
    private Direction direction;
    private String[] requiredItems;
    private short requiredExp;
    private String assetName;

    public DoorInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getRequiredItems() {
        return requiredItems;
    }

    public void setRequiredItems(String[] requiredItems) {
        this.requiredItems = requiredItems;
    }

    public short getRequiredExp() {
        return requiredExp;
    }

    public void setRequiredExp(short requiredExp) {
        this.requiredExp = requiredExp;
    }

    public String getEnteredRoom() {
        return enteredRoom;
    }

    public void setEnteredRoom(String enteredRoom) {
        this.enteredRoom = enteredRoom;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }
}
