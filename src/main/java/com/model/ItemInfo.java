package com.model;

/***
 * POJO class, represents single item info
 */

public class ItemInfo {

    private boolean disposable;
    private String imageAsset;
    private String description;
    private String name;

    public ItemInfo(boolean disposable, String imageAsset, String description, String name) {
        this.disposable = disposable;
        this.imageAsset = imageAsset;
        this.description = description;
        this.name = name;
    }

    public ItemInfo() {
    }

    public void setDisposable(boolean disposable) {
        this.disposable = disposable;
    }

    public void setImageAsset(String imageAsset) {
        this.imageAsset = imageAsset;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDisposable() {
        return disposable;
    }

    public String getImageAsset() {
        return imageAsset;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
}
