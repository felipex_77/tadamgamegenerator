package com.model;

import java.util.List;

public class LevelInfo {

    private String fullLevelName;
    private String firstRoom;
    private boolean successfulCompletionRequired;
    private boolean playerReset;
    private List<CupInfo> cups;

    public LevelInfo() {}

    public void setFirstRoom(String firstRoom) {
        this.firstRoom = firstRoom;
    }

    public String getFirstRoom() {
        return firstRoom;
    }

    public void setFullLevelName(String fullLevelName) {
        this.fullLevelName = fullLevelName;
    }

    public String getFullLevelName() {
        return fullLevelName;
    }

    public boolean isPlayerReset() {
        return playerReset;
    }

    public void setPlayerReset(boolean playerReset) {
        this.playerReset = playerReset;
    }

    public boolean isSuccessfulCompletionRequired() {
        return successfulCompletionRequired;
    }

    public void setSuccessfulCompletionRequired(boolean successfulCompletionRequired) {
        this.successfulCompletionRequired = successfulCompletionRequired;
    }

    public List<CupInfo> getCups() {
        return cups;
    }

    public void setCups(List<CupInfo> cups) {
        this.cups = cups;
    }
}


