package com.model;

public class NoSuchAnswerException extends RuntimeException{
    public NoSuchAnswerException(String msg){
        super(msg);
    }
}
