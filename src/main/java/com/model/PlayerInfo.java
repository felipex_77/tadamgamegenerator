package com.model;

public class PlayerInfo {
    private String textureImage;
    private int collisionRadius;
    private int maxHealth;

    public PlayerInfo() {
    }

    public String getTextureImage() {
        return textureImage;
    }

    public void setTextureImage(String textureImage) {
        this.textureImage = textureImage;
    }

    public int getCollisionRadius() {
        return collisionRadius;
    }

    public void setCollisionRadius(int collisionRadius) {
        this.collisionRadius = collisionRadius;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }
}
