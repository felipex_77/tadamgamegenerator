package com.model;

public enum QuestionDifficulty {
    EASY,
    MEDIUM,
    HARD
}
