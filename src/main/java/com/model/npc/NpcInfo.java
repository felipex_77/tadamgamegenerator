package com.model.npc;

import com.model.QuestionDifficulty;

import java.util.List;

public class NpcInfo {
    public String name;
    public String imageAsset;
    public short expReward;
    public short questionsLimit;
    public String[] requiredItems;
    public short requiredExp;
    public List<String> acceptedQuestionTags;
    public QuestionDifficulty acceptedDifficulty;
    public String dialogueAsset;
    public NpcType interactionType;
    public String[] itemsTaken;
    public String[] itemsReward;
    public boolean disappearAfterLastQuestion;
}
