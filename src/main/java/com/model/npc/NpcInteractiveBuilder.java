package com.model.npc;

import com.GameRunner;
import com.components.EqComponent;
import com.components.PlayerComponent;
import com.components.rewards.ExpReward;
import com.interactive.Interactive;
import com.interactive.ItemDealingInteractive;
import com.interactive.QuestInteractive;
import com.interactive.QuestionInteractive;

import java.util.Arrays;

import static com.almasb.fxgl.dsl.FXGL.getAssetLoader;

public class NpcInteractiveBuilder {

    public static Interactive buildNpcInteractive(NpcInfo npcInfo) {
        if (npcInfo.interactionType == NpcType.QUESTION_DEALER) {
            return buildQuestionInteractive(npcInfo);
        }
        else if (npcInfo.interactionType == NpcType.ITEM_DEALER) {
            return buildItemDealingInteractive(npcInfo);
        }
        else {
            return buildQuestInteractive(npcInfo);
        }
    }

    public static QuestionInteractive buildQuestionInteractive(NpcInfo npcInfo) {
        QuestionInteractive questionInteractive = new QuestionInteractive();
        questionInteractive.setReward(new ExpReward(npcInfo.expReward));
        questionInteractive.setQuestionsLimit(npcInfo.questionsLimit);
        questionInteractive.setAcceptedQuestionTags(npcInfo.acceptedQuestionTags);
        questionInteractive.setAcceptedDifficulty(npcInfo.acceptedDifficulty);
        questionInteractive.setDialogueGraph(getAssetLoader().loadDialogueGraph(npcInfo.dialogueAsset));
        questionInteractive.setNpcName(npcInfo.name);
        questionInteractive.setLastInteraction(npcInfo.disappearAfterLastQuestion);
        return questionInteractive;
    }

    public static ItemDealingInteractive buildItemDealingInteractive(NpcInfo npcInfo) {
        return new ItemDealingInteractive(GameRunner.getPlayer().getComponent(EqComponent.class),
                Arrays.asList(npcInfo.itemsTaken.clone()),
                Arrays.asList(npcInfo.itemsReward.clone()),
                getAssetLoader().loadDialogueGraph(npcInfo.dialogueAsset));
    }

    public static QuestInteractive buildQuestInteractive(NpcInfo npcInfo) {
        return new QuestInteractive(GameRunner.getPlayer().getComponent(EqComponent.class),
                GameRunner.getPlayer().getComponent(PlayerComponent.class),
                Arrays.asList(npcInfo.itemsTaken.clone()),
                Arrays.asList(npcInfo.itemsReward.clone()),
                getAssetLoader().loadDialogueGraph(npcInfo.dialogueAsset));
    }
}
