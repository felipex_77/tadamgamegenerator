package com.model.npc;

public enum NpcType {
    QUESTION_DEALER, ITEM_DEALER, QUEST_DEALER
}
