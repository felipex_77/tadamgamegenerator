package com.model.questions;

import com.model.Answer;
import com.model.QuestionDifficulty;

import java.util.*;

public class MultipleChoiceQuestion extends Question {
    private Set<Answer> goodAnswers;

    public MultipleChoiceQuestion(String questionText, List<Answer> answers, List<Answer> goodAnswers, QuestionDifficulty questionDifficulty, Set<String> tags) {
        super(questionText, answers, questionDifficulty, tags);
        this.goodAnswers = new HashSet<>(goodAnswers);
    }

    public MultipleChoiceQuestion(){}

    public boolean verify(Answer answer){
        return goodAnswers.contains(answer);
    }

    public void setGoodAnswer(Answer goodAnswer) {
        assert getAnswers().contains(goodAnswer): "Answer doesn't belong to this question.";
        this.goodAnswers.add(goodAnswer);
    }

    public void setGoodAnswers(Set<Answer> goodAnswers) {
        this.goodAnswers = goodAnswers;
    }

    public boolean allowsMultipleChoice(){return true;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MultipleChoiceQuestion that = (MultipleChoiceQuestion) o;
        return goodAnswers.equals(that.goodAnswers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), goodAnswers);
    }
}
