package com.model.questions;

import com.model.Answer;
import com.ui.QuestionDialogCreator;
import javafx.scene.Node;

import java.util.HashSet;
import java.util.Set;

public class MultipleChoiceQuestionInteraction extends QuestionInteraction {
    private Set<Answer> selectedAnswers = new HashSet<>();
    private Node uiView = QuestionDialogCreator.renderMultipleChoiceQuestionView(this);

    public MultipleChoiceQuestionInteraction(Question question) {
        super(question);
    }

    @Override
    public void select(Answer answer) {
        assert question.getAnswers().contains(answer): "Question doesn't contain such answer";
        selectedAnswers.add(answer);
    }

    @Override
    public void unselect(Answer answer) {
        assert question.getAnswers().contains(answer): "Question doesn't contain such answer";
        selectedAnswers.remove(answer);
    }

    @Override
    public Double evaluate() {
        long answeredCorrectly = selectedAnswers.stream().filter(question::verify).count();
        if (answeredCorrectly != selectedAnswers.size())
            return 0.0; // if any wrong answer was chosen
        long allGoodAnswers = question.getAnswers().stream().filter(question::verify).count();

        return answeredCorrectly/(double)allGoodAnswers;
    }

    @Override
    public boolean isSelected() {
        return !selectedAnswers.isEmpty();
    }

    @Override
    public void reset() {
        selectedAnswers.clear();
    }

    @Override
    public Node getUiView() {
        return uiView;
    }
}
