package com.model.questions;

import com.model.Answer;
import com.model.QuestionDifficulty;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/***
 * POJO class, represents single question with possible answer
 * subclasses may implement different verify strategy e.g. allow multiple good answers
 */

public abstract class Question {
    private String questionText;
    private List<Answer> answers;
    private QuestionDifficulty questionDifficulty;
    private Set<String> tags;

    public Question(String questionText, List<Answer> answers, QuestionDifficulty questionDifficulty, Set<String> tags) {
        this.questionText = questionText;
        this.answers = answers;
        this.questionDifficulty = questionDifficulty;
        this.tags = tags;
    }

    public Question(){}

    public String ask() {
        return questionText;
    }

    public void addAnswer(Answer answer, Boolean isGoodAnswer){
        answers.add(answer);
        if (isGoodAnswer)
            setGoodAnswer(answer);
    }

    public abstract boolean verify(Answer answer);

    public abstract void setGoodAnswer(Answer goodAnswer);

    public abstract boolean allowsMultipleChoice();

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public List<Answer> getAnswers(){
        return answers;
    }

    public void setQuestionDifficulty(QuestionDifficulty questionDifficulty) {
        this.questionDifficulty = questionDifficulty;
    }

    public QuestionDifficulty getQuestionDifficulty(){
        return questionDifficulty;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public Collection<String> getTags(){
        return tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return questionText.equals(question.questionText) && answers.equals(question.answers) && questionDifficulty == question.questionDifficulty && tags.equals(question.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionText, answers, questionDifficulty, tags);
    }
}
