package com.model.questions;

import com.model.Answer;
import javafx.scene.Node;

import java.util.List;

/***
 * Proxy used to communicate with question. Handles user actions such as answer selection.
 * @questions contains question for this interaction
 * @chosen contains currently chosen answer
 * @reward contains reward, which is returned according to principles defined in evaluate() function
 */

public abstract class QuestionInteraction {
    protected final Question question;

    public QuestionInteraction(Question question){
        this.question = question;
    }

    public String ask(){
        return question.ask();
    }

    public List<Answer> getAnswers(){
        return question.getAnswers();
    }

    public abstract void select(Answer answer);

    public abstract void unselect(Answer answer);

    /**
     * Defines logic of evaluating answer
     * @return percentage result
     */
    public abstract Double evaluate();

    public abstract boolean isSelected();

    public abstract void reset();

    public abstract Node getUiView();

    public Question getQuestion() {
        return question;
    }
}
