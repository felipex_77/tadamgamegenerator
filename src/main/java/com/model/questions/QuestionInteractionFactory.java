package com.model.questions;

public class QuestionInteractionFactory {
    public QuestionInteraction getStandardInteractionForQuestion(Question question){
        if (question.allowsMultipleChoice())
            return new MultipleChoiceQuestionInteraction(question);
        else
            return new SingleChoiceQuestionInteraction(question);
    }
}
