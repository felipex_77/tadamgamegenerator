package com.model.questions;

import com.model.Answer;
import com.model.QuestionDifficulty;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class SingleChoiceQuestion extends Question{
    private Answer goodAnswer;

    public SingleChoiceQuestion(String questionText, List<Answer> answers, Answer goodAnswer, QuestionDifficulty questionDifficulty, Set<String> tags) {
        super(questionText, answers, questionDifficulty, tags);
        this.goodAnswer = goodAnswer;
    }

    public SingleChoiceQuestion(){}

    public boolean verify(Answer answer){
        return answer.equals(goodAnswer);
    }

    public void setGoodAnswer(Answer goodAnswer) {
        assert getAnswers().contains(goodAnswer): "Answer doesn't belong to this question.";
        this.goodAnswer = goodAnswer;
    }

    @Override
    public boolean allowsMultipleChoice() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SingleChoiceQuestion that = (SingleChoiceQuestion) o;
        return goodAnswer.equals(that.goodAnswer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), goodAnswer);
    }
}
