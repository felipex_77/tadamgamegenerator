package com.model.questions;

import com.model.Answer;
import com.model.NoSuchAnswerException;
import com.ui.QuestionDialogCreator;
import javafx.scene.Node;

public class SingleChoiceQuestionInteraction extends QuestionInteraction {
    private Answer chosen;
    private final Node uiView = QuestionDialogCreator.renderSingleChoiceQuestionView(this);

    public SingleChoiceQuestionInteraction(Question question){
        super(question);
    }

    public void select(Answer answer){
        if (question.getAnswers().contains(answer)){
            chosen = answer;
        }
        else{
            throw new NoSuchAnswerException("Question must contain given answer!");
        }
    }

    public void unselect(Answer answer){
        if (chosen.equals(answer))
            chosen = null;
    }

    /**
     * Defines logic of evaluating answer
     * @return percentage result
     * @throws AssertionError informs user, that he didn't select any answer
     */

    public Double evaluate() {
        assert isSelected(): "Answer must be selected";
        if (question.verify(chosen)){
            return 1.0;
        }
        else {
            return 0.0;
        }
    }

    public boolean isSelected(){
        return chosen != null;
    }

    public void reset(){
        chosen = null;
    }

    @Override
    public Node getUiView() {
        return uiView;
    }
}
