package com.ui;

import com.components.EqComponent;
import com.components.items.Item;
import javafx.scene.image.ImageView;

import static com.almasb.fxgl.dsl.FXGLForKtKt.getAssetLoader;

public class EqItemView extends ImageView {
    private static final String EMPTY_PLACE = "empty_eq.png";

    public EqItemView(Item item, EqComponent eq, EqVBox box) {
        super();
        if (item == null) {
            withItemNull();
        }
        else {
            withItem(item, eq, box);
        }
    }

    private void withItemNull() {
        setImage(getAssetLoader().loadImage(EMPTY_PLACE));
    }

    private void withItem(Item item, EqComponent eq, EqVBox box) {
        String assetName = item.getAssetName();
        setImage(getAssetLoader().loadImage(assetName));
        setOnMouseClicked(event -> {
            eq.useItem(item);
            box.updateItems();
        });
    }
}
