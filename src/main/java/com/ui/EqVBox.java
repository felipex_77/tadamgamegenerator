package com.ui;

import com.components.EqComponent;
import com.components.items.Item;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;


public class EqVBox extends VBox {
    private final static int ELEMENTS_PER_ROW = 6;
    private final static int PADDING = 5;

    private final EqComponent eq;

    public EqVBox(EqComponent eq) {
        super();
        setSpacing(5);
        setAlignment(Pos.CENTER);
        setVisible(false);
        this.eq = eq;
        eq.addEqVBox(this);
        addTitleLabel();
        setBackground(new Background(new BackgroundFill(Paint.valueOf(Color.WHITE.toString()), null, null)));
        getChildren().add(getItemsView(eq.getEqSize()));
        setPrefWidth((PADDING+40)*ELEMENTS_PER_ROW+PADDING);
        setPrefHeight((PADDING+40)*(eq.getEqSize()/ELEMENTS_PER_ROW) + 80);
    }

    public void addTitleLabel() {
        Label label = new Label("Equipment");
        label.setFont(new Font("Arial", 24));
        getChildren().add(label);
    }

    private HBox getRow(int rowID) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(5);
        int elementsInRow = Math.min(ELEMENTS_PER_ROW, eq.getEqSize()-ELEMENTS_PER_ROW*rowID);
        for(int i=0; i<elementsInRow; i++) {
            hBox.getChildren().add(getEqElementImageView(rowID*ELEMENTS_PER_ROW+i));
        }
        return hBox;
    }

    private VBox getItemsView(int elements) {
        VBox vBox = new VBox();
        vBox.setSpacing(5);
        for (int i = 0; i < (float)(elements)/ELEMENTS_PER_ROW; i++) {
            HBox row = getRow(i);
            vBox.getChildren().add(row);
        }
        return vBox;
    }

    private ImageView getEqElementImageView(int eqElement) {
        Item item = eq.getItems().size() > eqElement? eq.getItems().get(eqElement) : null;
        return new EqItemView(item, eq, this);
    }

    public void changeVisibility() {
        if (isVisible()) {
            setVisible(false);
        }
        else {
            updateItems();
            setVisible(true);
        }
    }

    public void updateItems() {
        getChildren().remove(1);
        getChildren().add(getItemsView(eq.getEqSize()));
    }
}
