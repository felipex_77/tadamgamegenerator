package com.ui;

import com.GameRunner;
import com.components.StatisticsComponent;
import com.model.QuestionDifficulty;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;

import static com.almasb.fxgl.dsl.FXGL.getGameController;
import static com.almasb.fxgl.dsl.FXGLForKtKt.getDialogService;
import static com.almasb.fxgl.dsl.FXGLForKtKt.getUIFactoryService;

public class GameOverDialog {
    private final String message;
    private final HashMap<String, Float> scores;
    private final Instant startTime;
    private final StatisticsComponent stats;

    public GameOverDialog(String message, HashMap<String, Float> scores, Instant startTime, StatisticsComponent stats) {
        this.message = message;
        this.scores = scores;
        this.startTime = startTime;
        this.stats = stats;
    }

    public void show() {
        HBox buttons = initButtons();
        getDialogService().showBox(message, buttons);
    }

    private HBox initButtons() {
        Button statsBtn = getUIFactoryService().newButton("Statistics");
        Button restartBtn = getUIFactoryService().newButton("Play Again");
        Button menuBtn = getUIFactoryService().newButton("Menu");

        statsBtn.setOnMouseClicked(e -> showStatisticsDialog());
        restartBtn.setOnMouseClicked(e -> {
            GameRunner.setPlayer(null);
            getGameController().startNewGame();
        });
        menuBtn.setOnMouseClicked(e -> getGameController().gotoMainMenu());

        return new HBox(
                statsBtn,
                restartBtn,
                menuBtn
        );
    }

    private void showStatisticsDialog() {
        VBox content = new VBox(
                getUIFactoryService().newText("Time played: " + getElapsedTime()),
                getUIFactoryService().newText("TScores in levels:\n")
        );

        for (String levelName : scores.keySet()) {
            content.getChildren().add(getUIFactoryService().newText(levelName + ": " + scores.get(levelName) + "\n"));
        }

        for(QuestionDifficulty d: QuestionDifficulty.values()){
            int nOfCorrectAnswers = stats.getNOfCorrectAnswers(d);
            int nOfAllAnswers = stats.getNOfAnswers(d);
            double ratio = nOfAllAnswers == 0 ? 0 : (100.0 * nOfCorrectAnswers) / nOfAllAnswers;
            String text = d + ": " + nOfCorrectAnswers + "/" + nOfAllAnswers + String.format(" (%.2f)", ratio) + "%";
            content.getChildren().add(getUIFactoryService().newText(text));
        }

        Button btnClose = getUIFactoryService().newButton("OK");
        getDialogService().showBox("Game statistics", content, btnClose);
    }

    private String getElapsedTime() {
        Duration diff = Duration.between(startTime, Instant.now());
        String hours = diff.toHoursPart() >= 10 ? "" + diff.toHoursPart() : "0" + diff.toHoursPart();
        String minutes = diff.toMinutesPart() >= 10 ? "" + diff.toMinutesPart() : "0" + diff.toMinutesPart();
        String seconds = diff.toSecondsPart() >= 10 ? "" + diff.toSecondsPart() : "0" + diff.toSecondsPart();
        return hours + ":" + minutes + ":" + seconds;
    }
}
