package com.ui;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

/**
 * Class generates health points view and allows binding it with player health points, to update automatically
 */

public class HealthPointsBar extends HBox implements ChangeListener<Number> {
    private final IntegerProperty currentHealthPoints = new SimpleIntegerProperty(0);

    public HealthPointsBar(){
        currentHealthPoints.addListener(this);
        setTranslateX(50);
    }

    private void paintHealthPoints(int healthPoints){
        getChildren().clear();
        for (int i = 0; i<healthPoints; i++){
            this.getChildren().add(createHeart());
        }
    }

    private SVGPath createHeart(){
        String path = "M23.6,0c-3.4,0-6.3,2.7-7.6,5.6C14.7,2.7,11.8,0,8.4,0C3.8,0,0,3.8,0,8.4c0,9.4,9.5,11.9,16,21.2\n" +
                "c6.1-9.3,16-12.1,16-21.2C32,3.8,28.2,0,23.6,0z";
        SVGPath heartPath = new SVGPath();
        heartPath.setFill(Color.RED);
        heartPath.setContent(path);
        return heartPath;
    }

    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        paintHealthPoints(newValue.intValue());
    }

    public IntegerProperty currentHealthPointsProperty() {
        return currentHealthPoints;
    }
}
