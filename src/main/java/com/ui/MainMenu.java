package com.ui;

import com.Tutorial;
import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.MenuType;
import com.almasb.fxgl.dsl.FXGL;
import com.util.LineFormatter;
import javafx.beans.binding.Bindings;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static com.almasb.fxgl.dsl.FXGL.*;

public class MainMenu extends FXGLMenu {

    private final static int BASIC_ITEM_WIDTH = 200;
    private final static int BASIC_ITEM_HEIGHT = 50;
    private final static int BASIC_ITEM_OFFSET = 50;

    private String playerName = "player";
    private Path pathToConfigurationFolder;

    private final List<String> requiredConfigFiles;
    private static final Color darkGreen = Color.valueOf("4d8c85");
    private static final Color lightGreen = Color.valueOf("72ccc3");
    private static final Color backgroundColor = Color.valueOf("1c2123");

    public MainMenu(String defaultConfigurationFolder, List<String> requiredConfigFiles) {
        super(MenuType.MAIN_MENU);

        this.pathToConfigurationFolder = Path.of(defaultConfigurationFolder);
        this.requiredConfigFiles = requiredConfigFiles;

        var bg = new Rectangle(getAppWidth(), getAppHeight());
        bg.setFill(backgroundColor);
        getContentRoot().getChildren().add(bg);

        var title = FXGL.getUIFactoryService().newText("PLAY & LEARN IT", lightGreen, 28);
        title.resize(BASIC_ITEM_WIDTH, BASIC_ITEM_HEIGHT);
        title.setX(FXGL.getAppWidth() / 2.0 - BASIC_ITEM_WIDTH + 90);
        title.setY(BASIC_ITEM_OFFSET);

        var nameInput = createInputField(
                "player name",
                playerName,
                FXGL.getAppWidth() / 2.0 - BASIC_ITEM_WIDTH - BASIC_ITEM_OFFSET,
                FXGL.getAppHeight() / 2.0 - BASIC_ITEM_HEIGHT / 2.0
        );

        var assetsInput = createConfigurationField(
                "assets folder name",
                pathToConfigurationFolder.getFileName().toString(),
                FXGL.getAppWidth() / 2.0 + BASIC_ITEM_OFFSET,
                FXGL.getAppHeight() / 2.0 - BASIC_ITEM_HEIGHT / 2.0
        );

        var button = new GameButton("Start new game", nameInput, assetsInput, this, this::fireNewGame);
        button.setTranslateX(FXGL.getAppWidth() / 2.0 - BASIC_ITEM_WIDTH);
        button.setTranslateY(title.getTranslateY() + 2 * BASIC_ITEM_OFFSET);

        GameButton tutorialButton = new GameButton("Tutorial", nameInput, assetsInput, this, Tutorial::startTutorial);
        tutorialButton.setTranslateX(button.getTranslateX());
        tutorialButton.setTranslateY(button.getTranslateY()+ BASIC_ITEM_HEIGHT*3/2.0);

        getContentRoot().getChildren().addAll(title, button, tutorialButton);
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPathToConfigurationFolder(){
        return pathToConfigurationFolder.toString() + "/";
    }

    private TextField createInputField(String inputName, String initialValue, double xCoordinate, double yCoordinate) {
        // Frame for elements
        VBox content = new VBox();
        content.setSpacing(20);
        content.setTranslateX(xCoordinate);
        content.setTranslateY(yCoordinate);

        var title = FXGL.getUIFactoryService().newText("Provide " + inputName + ":", lightGreen, 14);
        title.resize(BASIC_ITEM_WIDTH, BASIC_ITEM_OFFSET);

        var input = new TextField();
        input.setPrefSize(BASIC_ITEM_WIDTH, BASIC_ITEM_HEIGHT);
        input.setText(initialValue);

        content.getChildren().addAll(title, input);
        getContentRoot().getChildren().add(content);
        return input;
    }

    private TextField createConfigurationField(String inputName, String initialValue, double xCoordinate, double yCoordinate){
        // Frame for elements
        VBox content = new VBox();
        content.setSpacing(20);
        content.setTranslateX(xCoordinate);
        content.setTranslateY(yCoordinate);

        var title = FXGL.getUIFactoryService().newText("Provide " + inputName + ":", lightGreen, 14);
        title.resize(BASIC_ITEM_WIDTH, BASIC_ITEM_OFFSET);

        var input = new TextField();
        input.setPrefSize(BASIC_ITEM_WIDTH/(float)2, BASIC_ITEM_HEIGHT);
        input.setText(initialValue);
        input.setEditable(false);

        var selectConfigurationButton = new Button("Select");
        selectConfigurationButton.setStyle("-fx-background-color: \"72ccc3\"; ");
        selectConfigurationButton.setPrefSize(BASIC_ITEM_WIDTH/(float)2, BASIC_ITEM_HEIGHT);
        selectConfigurationButton.setOnAction(event -> {
            var configFile = selectConfigurationDirectory();
            configFile.ifPresent(file -> {
                pathToConfigurationFolder = file.toPath();
                input.setText(pathToConfigurationFolder.getFileName().toString());
            });
        });

        HBox selectionLine = new HBox();
        selectionLine.getChildren().addAll(input, selectConfigurationButton);

        content.getChildren().addAll(title, selectionLine);
        getContentRoot().getChildren().add(content);
        return input;
    }

    private Optional<File> selectConfigurationDirectory(){
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(getPrimaryStage());
        return Optional.ofNullable(selectedDirectory);
    }

    private List<String> getMissingConfigFiles() throws IOException {
        Set<String> files = new HashSet<>();
        List<String> missingConfigFiles = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(pathToConfigurationFolder)) {
            for (Path path : stream) {
                if (!Files.isDirectory(path)) {
                    files.add(path.toFile().getName());
                }
            }
        }
        for (var requiredFile : requiredConfigFiles) {
            if (!files.contains(requiredFile)){
                missingConfigFiles.add(requiredFile);
            }
        }
        return missingConfigFiles;
    }

    private void informAboutLackOfConfigFiles(List<String> lackingFiles){
        var message = "Folder "+
                getPathToConfigurationFolder() + " do not contain required json files: " +
                String.join(", ", lackingFiles);
        getDialogService().showMessageBox(LineFormatter.formatLines(message, 50), () -> getGameController().gotoMainMenu());
    }

    private void informAboutWrongPathToConfigurations(){
        var message = "Folder "
                + getPathToConfigurationFolder() + " do not exists";
        getDialogService().showMessageBox(LineFormatter.formatLines(message, 50), () -> getGameController().gotoMainMenu());
    }

    private static class GameButton extends StackPane {
        public GameButton(String name, TextField nameInput, TextField assetsInput, MainMenu mainMenu, Runnable action) {

            var bg = new Rectangle(2 * BASIC_ITEM_WIDTH, BASIC_ITEM_HEIGHT);
            bg.setStroke(lightGreen);

            var text = FXGL.getUIFactoryService().newText(name, backgroundColor, 18);

            bg.fillProperty().bind(
                    Bindings.when(hoverProperty()).then(lightGreen).otherwise(darkGreen)
            );

            text.fillProperty().bind(
                    Bindings.when(hoverProperty()).then(darkGreen).otherwise(lightGreen)
            );

            setOnMouseClicked(e -> {
                mainMenu.playerName = nameInput.getText();
                try {
                    var missingConfigurationFiles = mainMenu.getMissingConfigFiles();
                    if (missingConfigurationFiles.isEmpty()) {
                        action.run();
                    } else {
                        mainMenu.informAboutLackOfConfigFiles(missingConfigurationFiles);
                    }
                } catch (IOException ioException) {
                    mainMenu.informAboutWrongPathToConfigurations();
                }
            });

            getChildren().addAll(bg, text);
        }
    }
}
