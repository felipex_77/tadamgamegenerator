package com.ui;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

import java.util.List;

import static com.almasb.fxgl.dsl.FXGL.getAppHeight;
import static com.almasb.fxgl.dsl.FXGL.getAppWidth;

public class OfferBox extends VBox {

    public OfferBox(List<String> requestedItems, List<String> offeredItems) {
        super();
        getChildren().addAll(getTitleLabel(), getContent(requestedItems, offeredItems));
        setBackground(new Background(new BackgroundFill(Paint.valueOf(Color.WHITE.toString()), null, null)));
        setAlignment(Pos.CENTER);
        setSpacing(10);
        setPrefWidth(300);
        setTranslateX((getAppWidth() - getPrefWidth()) / 2);
        setTranslateY((getAppHeight() - getMinHeight()) / 2 - 40);
    }

    public HBox getContent(List<String> requestedItems, List<String> offeredItems) {
        HBox box = new HBox();
        VBox demandsBox = generateOffer("NPC will get:", requestedItems);
        VBox offerBox = generateOffer("Player will get:", offeredItems);
        box.getChildren().addAll(demandsBox, offerBox);
        box.setAlignment(Pos.CENTER);
        box.setSpacing(30);
        return box;
    }

    private VBox generateOffer(String title, List<String> elementsList) {
        VBox box = new VBox();
        box.setAlignment(Pos.TOP_CENTER);
        box.getChildren().add(getTitleLabel(title));
        elementsList.stream().map(Label::new).forEach(label -> box.getChildren().add(label));
        return box;
    }

    private Label getTitleLabel(String title) {
        Label label = new Label(title);
        label.setFont(new Font("Arial", 20));
        return label;
    }

    private Label getTitleLabel() {
        Label label = new Label("Merchandise offer:");
        label.setFont(new Font("Arial", 24));
        return label;
    }
}
