package com.ui;

import com.interactive.QuestionInteractive;
import com.model.questions.QuestionInteraction;
import com.util.LineFormatter;
import javafx.scene.Node;
import javafx.scene.control.Button;

import static com.almasb.fxgl.dsl.FXGL.getDialogService;
import static com.almasb.fxgl.dsl.FXGL.getUIFactoryService;

public class QuestionDialog {
    private final QuestionInteraction interaction;
    private final QuestionInteractive resultObserver;

    public QuestionDialog(QuestionInteraction interaction, QuestionInteractive resultObserver) {
        this.interaction = interaction;
        this.resultObserver = resultObserver;
    }

    public void show() {
        Button closeButton = initCloseButton();
        Node questionContent = initAnswersView();
        String questionText = interaction.ask();
        getDialogService().showBox(LineFormatter.formatLines(questionText, 50), questionContent, closeButton);
    }

    private Button initCloseButton() {
        Button closeButton = getUIFactoryService().newButton("Zatwierdź");
        closeButton.setOnAction(event -> resultObserver.answeringFinished());
        return closeButton;
    }

    private Node initAnswersView() {
        return interaction.getUiView();
    }

}
