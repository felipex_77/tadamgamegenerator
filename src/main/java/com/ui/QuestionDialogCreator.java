package com.ui;

import com.model.Answer;
import com.model.questions.QuestionInteraction;
import com.util.LineFormatter;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static com.almasb.fxgl.dsl.FXGL.getUIFactoryService;

public class QuestionDialogCreator {
    public static Node renderMultipleChoiceQuestionView(QuestionInteraction interaction){
        VBox questionContent = new VBox();
        for (Answer answer : interaction.getAnswers()) {
            HBox line = new HBox();
            CheckBox answerButton = new CheckBox();
            answerButton.setUserData(answer);
            answerButton.setOnAction(event -> {
                if (answerButton.isSelected()) {
                    interaction.select((Answer) answerButton.getUserData());
                }
                else
                    interaction.unselect((Answer) answerButton.getUserData());
            });
            line.getChildren().addAll(answerButton, getUIFactoryService().newText(LineFormatter.formatLines(answer.getText(), 50)));
            questionContent.getChildren().add(line);
        }
        return questionContent;
    }

    public static Node renderSingleChoiceQuestionView(QuestionInteraction interaction){
        ToggleGroup answersGroup = new ToggleGroup(); // allows only one button to be chosen
        VBox questionContent = new VBox();
        for (Answer answer : interaction.getAnswers()) {
            HBox line = new HBox();
            RadioButton answerButton = new RadioButton();
            answerButton.setUserData(answer);
            answerButton.setOnAction(event -> {
                if (answerButton.isSelected()) {
                    interaction.select((Answer) answerButton.getUserData());
                }
            });
            answerButton.setToggleGroup(answersGroup);
            line.getChildren().addAll(answerButton, getUIFactoryService().newText(LineFormatter.formatLines(answer.getText(), 50)));
            questionContent.getChildren().add(line);
        }
        return questionContent;
    }

}
