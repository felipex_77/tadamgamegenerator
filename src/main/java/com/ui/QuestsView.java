package com.ui;

import com.almasb.fxgl.dsl.FXGL;
import com.model.ItemInfo;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.util.LinkedList;
import java.util.List;

public class QuestsView extends VBox {
    private static final String QUEST_FORMAT = "Musisz znaleźć %s!";


    public QuestsView() {
        setSpacing(5);
        setVisible(false);
        setAlignment(Pos.CENTER);
        setBackground(new Background(new BackgroundFill(Paint.valueOf(Color.WHITE.toString()), null, null)));
    }


    public void switchVisibility(List<ItemInfo> data) {
        if (isVisible()) {
            setVisible(false);
        } else {
            buildView(data);
            setVisible(true);
        }
    }

    private void buildView(List<ItemInfo> data) {
        List<Node> newNodes = new LinkedList<>();
        newNodes.add(buildTitle());
        data.stream().map(itemInfo -> {
            String questDescription = String.format(QUEST_FORMAT, itemInfo.getName());
            Text questText = FXGL.getUIFactoryService().newText(questDescription);
            questText.setFill(Color.BLACK);
            var questView = new HBox(10, questText,
                    FXGL.texture(itemInfo.getImageAsset(), 40, 40));
            questView.setAlignment(Pos.CENTER_LEFT);
            return questView;
        }).forEach(newNodes::add);
        getChildren().setAll(newNodes);
        setTranslateX(0);
        setTranslateY(0);
    }

    private Node buildTitle() {
        Text title = FXGL.getUIFactoryService().newText("Quests", 24);
        title.setFill(Color.BLACK);
        return title;
    }
}
