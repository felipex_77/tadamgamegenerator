package com.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class JsonLoader {
    private static ObjectMapper mapper = new ObjectMapper();

    public static <T> Optional<T> loadJSON(String path, Class<T> toClass){
        T value;
        try {
            value = mapper.readValue(new File(path), toClass);
        } catch (IOException e) {
            throw  new RuntimeException(e.getMessage());
        }
        return Optional.ofNullable(value);
    }
}
