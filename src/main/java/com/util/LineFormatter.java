package com.util;

public class LineFormatter {
    public static String formatLines(String text, int maxWidth){
        StringBuilder builder = new StringBuilder();
        String remainingText = text;
        while (remainingText.length() > maxWidth){
            String line = remainingText.substring(0, maxWidth);
            int nearestSpace = line.lastIndexOf(' ');
            if (nearestSpace >= 0){
                builder.append(line, 0, nearestSpace+1).append("\n");
                try {
                    remainingText = remainingText.substring(nearestSpace + 1);
                }catch (IndexOutOfBoundsException e){
                    remainingText = "";
                }
            }
            else{
                builder.append(line, 0, maxWidth-1).append('-').append("\n");
                remainingText = remainingText.substring(maxWidth-1);
            }
        }
        builder.append(remainingText);
        return builder.toString();
    }
}
