open module tadam.game.generator.main {
    requires com.almasb.fxgl.all;
    requires com.fasterxml.jackson.databind;
    requires annotations;
}