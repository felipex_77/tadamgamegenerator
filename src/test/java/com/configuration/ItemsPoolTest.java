package com.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.ItemInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class ItemsPoolTest {

    ItemsPool pool = new ItemsPool();

    @BeforeEach
    public void resetPool() {
        var mapper = new ObjectMapper();
        var defaultQuestionsPath = "src/test/resources/items.json";
        try {
            pool = mapper.readValue(new File(defaultQuestionsPath), ItemsPool.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void itemsLoadedProperly() {
        // When
        Optional<ItemInfo> expItemInfo = pool.getItem("magic-book");
        Optional<ItemInfo> healthItemInfo = pool.getItem("health-potion");
        Optional<ItemInfo> shouldNotExist = pool.getItem("dummy");
        // Then
        assertEquals("exp-item", expItemInfo.get().getName());
        assertEquals("health-item", healthItemInfo.get().getName());
        assertFalse(shouldNotExist.isPresent());
    }
}
