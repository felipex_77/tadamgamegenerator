package com.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.questions.Question;
import com.model.QuestionDifficulty;
import com.model.questions.SingleChoiceQuestion;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class QuestionsPoolTest {
    QuestionsPool pool;

    @BeforeEach
    public void resetPool(){
        var mapper = new ObjectMapper();
        var defaultQuestionsPath = "src/test/resources/questions.json";
        try{
            SingleChoiceQuestion[] questions = mapper.readValue(new File(defaultQuestionsPath), SingleChoiceQuestion[].class);
            pool = new QuestionsPool(new ArrayList<>(Arrays.asList(questions)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void takeQuestionWithoutFiltering(){
        // When
        Optional<Question> question = pool.take();
        // Then
        assertTrue(question.isPresent(), "Any questions should be taken.");
    }

    @Test
    public void takeQuestionWithValidTagFiltering(){
        // Given
        var validTags = Arrays.asList("polska", "rzeka", "niemcy");
        // When
        Optional<Question> question = pool.take(validTags);
        // Then
        assertTrue(question.isPresent());
        assertTrue(question.get().getTags().stream().anyMatch(validTags::contains), "Any question tag should be contained in queried tags");
    }

    @Test
    public void takeQuestionWithInvalidTagFiltering(){
        // Given
        var invalidTags = Arrays.asList("niemcy");
        // When
        Optional<Question> question = pool.take(invalidTags);
        // Then
        assertFalse(question.isPresent(), "No question should be selected for invalid task");
    }

    @Test
    public void takeQuestionWithDifficultyLevel(){
        // Given
        var difficultyLevel = QuestionDifficulty.EASY;
        // When
        Optional<Question> question = pool.take(difficultyLevel);
        // Then
        assertTrue(question.isPresent());
        assertEquals(difficultyLevel, question.get().getQuestionDifficulty(), "Difficulty level should be "+difficultyLevel);
    }

    @Test
    public void takeQuestionWithInvalidDifficultyLevel(){
        // Given
        var difficultyLevel = QuestionDifficulty.HARD;
        // When
        Optional<Question> question = pool.take(difficultyLevel);
        // Then
        assertFalse(question.isPresent(), difficultyLevel+" question should not be in pool");
    }

    @Test
    public void takeQuestionWithDifficultyAndFilters(){
        // Given
        var difficultyLevel = QuestionDifficulty.MEDIUM;
        var tags = Arrays.asList("polska", "góra");
        // When
        Optional<Question> question = pool.take(difficultyLevel, tags);
        // Then
        assertTrue(question.isPresent());
        assertEquals(difficultyLevel, question.get().getQuestionDifficulty());
        assertTrue(question.get().getTags().stream().anyMatch(tags::contains));
    }

    @Test
    public void takeNonMatchingQuestion(){
        // Given
        var difficultyLevel = QuestionDifficulty.MEDIUM;
        var tags = Arrays.asList("miasto");
        // When
        Optional<Question> question = pool.take(difficultyLevel, tags);
        // Then
        assertFalse(question.isPresent(), "Question with difficulty "+difficultyLevel+" and filter "+tags);
    }

    @Test
    public void takeOverLimit(){
        // Given
        var overallQuestionsNumber = pool.getQuestionsNumber();
        var numberOfQuestionsToTake = overallQuestionsNumber+1;
        List<Question> takenQuestions = new ArrayList<>();
        // When
        for (int i = 0; i < numberOfQuestionsToTake; i++){
            Optional<Question> taken = pool.take();
            taken.ifPresent(takenQuestions::add);
        }
        // Then
        assertEquals(numberOfQuestionsToTake, takenQuestions.size(), numberOfQuestionsToTake+" should be taken.");
    }

    @Test
    public void returnQuestionTest(){
        // Given
        List<Question> takenQuestions = new ArrayList<>();
        Optional<Question> takenAfterReturn;
        var size = pool.getQuestionsNumber();
        // When
        for (int i = 0; i < size; i++){
            Optional<Question> taken = pool.take();
            taken.ifPresent(takenQuestions::add);
        }
        // pool should now have only questions in taken group
        pool.returnQuestion(takenQuestions.get(takenQuestions.size()-1));
        takenAfterReturn = pool.take();
        // Then
        assertTrue(takenAfterReturn.isPresent());
        assertEquals(takenQuestions.get(takenQuestions.size()-1), takenAfterReturn.get());
    }
}
