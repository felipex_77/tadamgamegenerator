package com.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.questions.Question;
import com.model.questions.SingleChoiceQuestion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QuestionLoadTest {

    @Test
    public void simpleQuestionLoad(){
        // Given
        Question expectedQuestion = new SingleChoiceQuestion("Jakie miasto jest stolicą Polski?",
                Stream.of("Kraków", "Warszawa", "Katowice", "Gdańsk").map(Answer::new).collect(Collectors.toList()),
                new Answer("Warszawa"),
                QuestionDifficulty.EASY,
                new HashSet<>(Arrays.asList("geografia", "polska", "miasto")));
        ObjectMapper mapper = new ObjectMapper();

        // When
        Question actualQuestion = null;
        try {
            actualQuestion = mapper.readValue(new File("src/test/resources/question.json"), SingleChoiceQuestion.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Then
        Assertions.assertNotNull(actualQuestion);
        Assertions.assertEquals(expectedQuestion, actualQuestion);
    }
}
