package com.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.questions.Question;
import com.model.questions.SingleChoiceQuestion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QuestionsLoadTest {
    @Test
    public void simpleQuestionLoad(){
        // Given
        Question expectedQuestion = new SingleChoiceQuestion("Jakie miasto jest stolicą Polski?",
                Stream.of("Kraków", "Warszawa", "Katowice", "Gdańsk").map(Answer::new).collect(Collectors.toList()),
                new Answer("Warszawa"),
                QuestionDifficulty.EASY,
                new HashSet<>(Arrays.asList("geografia", "polska", "miasto")));
        ObjectMapper mapper = new ObjectMapper();

        // When
        List<SingleChoiceQuestion> questions = null;
        try {
            questions = mapper.readValue(new File("src/test/resources/new_load_questions.json"), new TypeReference<>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Then
        Assertions.assertNotNull(questions);
        Assertions.assertEquals(2, questions.size());
    }
}
