package com.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LineFormatterTest {
    @Test
    public void emptyTextTest(){
        // Given
        String input = "";
        String expected = "";

        // When
        String output = LineFormatter.formatLines(input, 10);

        // Then
        assertEquals(expected, output);
    }

    @Test
    public void onlySpaceTest(){
        // Given
        String input = " ";
        String expected = " ";

        // When
        String output = LineFormatter.formatLines(input, 10);

        // Then
        assertEquals(expected, output);
    }

    @Test
    public void oneLineTest(){
        // Given
        String input = "ala ma kota";
        String expected = "ala ma kota";

        // When
        String output = LineFormatter.formatLines(input, 11);

        // Then
        assertEquals(expected, output);
    }

    @Test
    public void multipleLinesTest(){
        // Given
        String input = "ala ma kota, a kot ma ale";
        String expected = "ala ma kota, \na kot ma ale";

        // When
        String output = LineFormatter.formatLines(input, 13);

        // Then
        assertEquals(expected, output);
    }

    @Test
    public void tooLongLineTest(){
        // Given
        String input = "thislineistoolongandshouldbesplitted";
        String expected = "thislineistool-\nongandshouldbe-\nsplitted";

        // When
        String output = LineFormatter.formatLines(input, 15);

        // Then
        assertEquals(expected, output);
    }
}
